package com.museum.projection.model;

import java.util.List;

/**
 * Database model
 */
public enum TrackTypes {
    picture(1), video(2), audio(3);

    final int id;

    TrackTypes(int id) {
        this.id = id;
    }

    public static TrackTypes of(int i) {
        switch (i) {
            case 1:
                return TrackTypes.picture;
            case 2:
                return TrackTypes.video;
            case 3:
                return TrackTypes.audio;
            default:
                throw new IllegalArgumentException("id " + i + " not recognized");
        }
    }

    public static List<TrackTypes> getListOfTypes() {
        return  List.of(TrackTypes.picture, TrackTypes.video, TrackTypes.audio);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return this.name();
    }


}
