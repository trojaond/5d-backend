package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Database model
 */
public class Videofile {

    @NotNull
    private final Integer id;

    @NotNull
    @NotBlank
    private final String filename;

    @NotNull
    @NotBlank
    private final String filepath;

    @NotNull
    private final Integer createdBy;

    @NotNull
    private final Timestamp createdCet;

    @NotNull
    private final Boolean clientSynchronized;

    private final @NotNull Float duration;

    @NotNull
    private final Long fileSize;

    private final String description;

    @NotNull
    private final Integer videoFileTypeId;

    @NotNull
    private final Integer folderId;

    public Videofile(@JsonProperty("videoFileId") Integer id,
                     @JsonProperty("filename") String filename,
                     @JsonProperty("filePath") String filepath,
                     @JsonProperty("createdBy") Integer createdBy,
                     @JsonProperty("createdCet") Timestamp createdCet,
                     @JsonProperty("clientSynchronized") Boolean clientSynchronized,
                     @JsonProperty("duration") Float duration,
                     @JsonProperty("fileSize") Long fileSize,
                     @JsonProperty("description") String description,
                     @JsonProperty("trackType") Integer videoFileTypeId,
                     @JsonProperty("folderId") Integer folderId) {
        this.id = id;
        this.filename = filename;
        this.filepath = filepath;
        this.createdCet = createdCet;
        this.createdBy = createdBy;
        this.clientSynchronized = clientSynchronized;
        this.duration = duration;
        this.fileSize = fileSize;
        this.description = description;
        this.videoFileTypeId = videoFileTypeId;
        this.folderId = folderId;
    }


    public Integer getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public Timestamp getCreatedCet() {
        return createdCet;
    }

    public Boolean getClientSynchronized() {
        return clientSynchronized;
    }

    public Float getDuration() {
        return duration;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public String getDescription() {
        return description;
    }

    public String getFilepath() {
        return filepath;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public Integer getVideoFileTypeId() {
        return videoFileTypeId;
    }
}
