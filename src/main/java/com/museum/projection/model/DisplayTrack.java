package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Database model
 */
public class DisplayTrack {

    @NotBlank
    private final Integer id;

    @NotBlank
    private final Integer displaySetId;

    @NotBlank
    private final String name;

    @NotBlank
    private final Integer createdBy;

    @NotBlank
    private final Timestamp createdCet;

    @NotNull
    private final Float duration;

    private final String description;

    @NotNull
    private final String trackType;

    public DisplayTrack(@JsonProperty("id") Integer id,
                        @JsonProperty("displaySetId") Integer displaySetId,
                        @JsonProperty("name") String name,
                        @JsonProperty("createdBy") Integer createdBy,
                        @JsonProperty("createdCet") Timestamp createdCet,
                        @JsonProperty("duration") Float duration,
                        @JsonProperty("description") String description,
                        @JsonProperty("trackType") String trackType) {
        this.id = id;
        this.displaySetId = displaySetId;
        this.name = name;
        this.createdCet = createdCet;
        this.createdBy = createdBy;
        this.duration = duration;
        this.description = description;
        this.trackType = trackType;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public Timestamp getCreatedCet() {
        return createdCet;
    }

    public Integer getDisplaySetId() {
        return displaySetId;
    }

    public Float getDuration() {
        return duration;
    }

    public String getTrackType() {
        return trackType;
    }

    public String getDescription() {
        return description;
    }
}
