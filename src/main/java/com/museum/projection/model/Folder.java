package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Database model
 */
public class Folder {

    @NotNull
    private final Integer id;

    private final Integer parentFolderId;

    @NotBlank
    private final String path;

    public Folder(@NotNull @JsonProperty("id") Integer id, @JsonProperty("parentFolderId") Integer parentFolderId, @NotBlank @JsonProperty("path") String path) {
        this.id = id;
        this.parentFolderId = parentFolderId;
        this.path = path;
    }


    public Integer getId() {
        return id;
    }

    public Integer getParentFolderId() {
        return parentFolderId;
    }

    public String getPath() {
        return path;
    }
}
