package com.museum.projection.dao;

import com.museum.projection.model.Folder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Folder DAO service
 */
@Service
public class FolderDaoService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public FolderDaoService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<Folder> getFolderById(int id) {
        final String sql = "SELECT id, parentFolderId, path FROM folder WHERE id = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            return resultSetToFolder(resultSet);
        }));
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<Folder> tryGetFolderById(int id) {
        final String sql = "SELECT id, parentFolderId, path FROM folder WHERE id = ?";
        List<Folder> folders = jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            return resultSetToFolder(resultSet);
        });
        if (folders.size() == 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(folders.get(0));
    }

    /**
     *
     * @param name
     * @param parentId
     * @return
     */
    public Optional<Folder> tryGetFolderByNameAndParentId(String name, int parentId) {
        final String sql = "SELECT id, parentFolderId, path FROM folder WHERE path = ? AND parentfolderid = ?";
        List<Folder> folders = jdbcTemplate.query(sql, new Object[]{name, parentId}, (resultSet, i) -> {
            return resultSetToFolder(resultSet);
        });
        if (folders.size() == 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(folders.get(0));
    }

    /**
     *
     * @return
     */
    public List<Folder> getFolders() {
        final String sql = "SELECT id, parentFolderId, path FROM folder ORDER BY id";
        List<Folder> folders = jdbcTemplate.query(sql, (resultSet, i) -> {
            return resultSetToFolder(resultSet);
        });
        return folders;
    }

    /**
     *
     * @param path
     * @param parentFolderId
     * @return
     */
    public Optional<Boolean> insertFolder(String path, Integer parentFolderId) {
        final String SQL = "INSERT INTO folder (parentFolderId, path) "
                + "VALUES(?,?)";
        return Optional.of(jdbcTemplate.update(SQL, parentFolderId, path) > 0);
    }

    private Folder resultSetToFolder(ResultSet resultSet) {
        try {
            return new Folder(resultSet.getInt("id"),
                    resultSet.getInt("parentFolderId"),
                    resultSet.getString("path"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}