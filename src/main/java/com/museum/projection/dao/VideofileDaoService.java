package com.museum.projection.dao;

import com.museum.projection.model.Videofile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Videofile DAO service
 */
@Service
public class VideofileDaoService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public VideofileDaoService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<Videofile> getVideoFileById(int id) {
        final String sql = "SELECT videoFileId, filename, createdby, createdcet, clientSynchronized, duration, fileSize, filepath, description, folderId, videoFileType FROM videofile WHERE videoFileId = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            return resultSetToVideoFile(resultSet);
        }));
    }

    /**
     *
     * @return
     */
    public List<Videofile> getVideoFiles() {
        final String sql = "SELECT videoFileId, filename, createdby, createdcet, clientSynchronized, duration, fileSize, filepath,description, folderId, videoFileType  FROM videofile ORDER BY videoFileId";
        List<Videofile> videofiles = jdbcTemplate.query(sql, (resultSet, i) -> {
            return resultSetToVideoFile(resultSet);
        });
        return videofiles;
    }

    /**
     *
     * @param filename
     * @param filePath
     * @param description
     * @param createdBy
     * @param clientSynchronized
     * @param duration
     * @param fileSize
     * @param folderId
     * @param trackType
     * @return
     */
    public Optional<Boolean> insertVideoFile(String filename, String filePath, String description, int createdBy, boolean clientSynchronized, float duration, long fileSize, int folderId, int trackType) {
        final String SQL = "INSERT INTO videofile (filename, filepath, description, createdBy, createdcet, clientSynchronized, duration, fileSize, folderId, videoFileType) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        return Optional.of(jdbcTemplate.update(SQL, filename, filePath, description, createdBy, Timestamp.valueOf(LocalDateTime.now(ZoneId.of("CET"))), clientSynchronized, duration, fileSize, folderId,trackType) > 0);
    }

    private Videofile resultSetToVideoFile(ResultSet resultSet) {
        try {
            return new Videofile(resultSet.getInt("videoFileId"),
                    resultSet.getString("filename"),
                    resultSet.getString("filePath"),
                    resultSet.getInt("createdBy"),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getBoolean("clientSynchronized"),
                    resultSet.getFloat("duration"),
                    resultSet.getLong("fileSize"),
                    resultSet.getString("description"),
                    resultSet.getInt("videoFileType"),
                    resultSet.getInt("folderId"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param videoFileId
     * @return
     */
    public Optional<Boolean> removeVideoFile(int videoFileId) {
        final String SQL = "DELETE FROM videofile WHERE videoFileId = ?";
        return Optional.of(jdbcTemplate.update(SQL, videoFileId) > 0);
    }
}