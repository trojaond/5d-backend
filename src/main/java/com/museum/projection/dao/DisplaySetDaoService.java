package com.museum.projection.dao;

import com.museum.projection.model.DisplaySet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Display set DAO service
 */
@Service
public class DisplaySetDaoService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DisplaySetDaoService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<DisplaySet> getDisplaySetById(int id) {
        final String sql = "SELECT id, name, createdby, createdcet, published, autonomous, duration, description, changed FROM displayset WHERE id = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            return new DisplaySet(Integer.valueOf(resultSet.getString("id")),
                    resultSet.getString("name"),
                    Integer.valueOf(resultSet.getString("createdBy")),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getFloat("duration"),
                    resultSet.getBoolean("published"),
                    resultSet.getBoolean("changed"),
                    resultSet.getInt("autonomous"),
                    resultSet.getString("description"));
        }));
    }

    /**
     *
     * @return
     */
    public List<DisplaySet> getDisplaySets() {
        final String sql = "SELECT id, name, createdby, createdcet, published, autonomous, duration, description, changed FROM displayset ORDER BY id";
        List<DisplaySet> displaySets = jdbcTemplate.query(sql, (resultSet, i) -> {
            return new DisplaySet(Integer.valueOf(resultSet.getString("id")),
                    resultSet.getString("name"),
                    Integer.valueOf(resultSet.getString("createdBy")),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getFloat("duration"),
                    resultSet.getBoolean("published"),
                    resultSet.getBoolean("changed"),
                    resultSet.getInt("autonomous"),
                    resultSet.getString("description"));
        });
        return displaySets;
    }

    /**
     *
     * @param name
     * @param createdBy
     * @param published
     * @param autonomous
     * @param duration
     * @param description
     * @param changed
     * @return
     */
    public Optional<Long> insertDisplaySet(String name, int createdBy, boolean published, int autonomous, float duration, String description, boolean changed) {
        final String SQL = "INSERT INTO displayset (name, createdBy, createdcet, published, autonomous, duration, description, changed) "
                + "VALUES(?,?,?,?,?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
            ps.setString(1, name);
            ps.setLong(2, createdBy);
            ps.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now(ZoneId.of("CET"))));
            ps.setBoolean(4, published);
            ps.setInt(5, autonomous);
            ps.setFloat(6, duration);
            ps.setString(7, description);
            ps.setBoolean(8, changed);
            return ps;
        }, keyHolder);
        return Optional.of((Long) Objects.requireNonNull(keyHolder.getKey()));
    }

    /**
     *
     * @param id
     * @param name
     * @param published
     * @param autonomous
     * @param changed
     * @param duration
     * @return
     */
    public Optional<Boolean> updateDisplaySet(String id, String name, boolean published, int autonomous, boolean changed, float duration) {
        final String SQL = "UPDATE displayset SET name = ?, published = ? , autonomous = ? , changed = ?, duration = ?"
                + "WHERE id = ?";
        return Optional.of(jdbcTemplate.update(SQL, name, published, autonomous, changed, duration, Long.parseLong(id)) > 0);
    }

}