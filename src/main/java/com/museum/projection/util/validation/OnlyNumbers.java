package com.museum.projection.util.validation;

import com.museum.projection.util.validation.validators.OnlyNumbersValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE ,CONSTRUCTOR, TYPE_USE})
@Retention(RUNTIME)

@Constraint(validatedBy = OnlyNumbersValidator.class)
@Documented
public @interface OnlyNumbers {

    String message() default "input is not a valid number";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}

