package com.museum.projection.util.validation.validators;

import com.museum.projection.util.validation.OnlyText;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator
 */
public class OnlyTextValidator implements ConstraintValidator<OnlyText, String> {


    @Override
    public void initialize(OnlyText constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = s == null || s.matches("[a-zA-Z_0-9\\s]*");

        if (!isValid) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(
                    "field must not contain characters other than [a-zA-Z_0-9] "
            )
                    .addConstraintViolation();
        }
        return isValid;
    }

    private String string;


}