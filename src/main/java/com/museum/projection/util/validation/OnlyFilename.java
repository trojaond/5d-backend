package com.museum.projection.util.validation;

import com.museum.projection.util.validation.validators.FilenameValidator;
import com.museum.projection.util.validation.validators.OnlyTextValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = FilenameValidator.class)
@Documented
public @interface OnlyFilename {

    String message() default "message from my new validator";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}