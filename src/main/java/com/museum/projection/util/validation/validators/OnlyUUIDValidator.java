package com.museum.projection.util.validation.validators;

import com.museum.projection.util.validation.OnlyUUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator
 */
public class OnlyUUIDValidator implements ConstraintValidator<OnlyUUID, String> {

    private String message;

    @Override
    public void initialize(OnlyUUID constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = s != null && s.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}");

        if (!isValid) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(this.message)
                    .addConstraintViolation();
        }
        return isValid;
    }

    private String string;


}