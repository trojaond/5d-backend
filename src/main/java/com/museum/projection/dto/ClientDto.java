
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class ClientDto {
    private int id;
    private String ip;
    private int port;
    private boolean isConnected;
    private boolean isPlaying;

    public ClientDto(int id, String ip, int port, boolean isConnected, boolean isPlaying) {
        this.id = id;
        this.ip = ip;
        this.port = port;
        this.isConnected = isConnected;
        this.isPlaying = isPlaying;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}






