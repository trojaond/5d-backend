package com.museum.projection.dto.forms;

import com.museum.projection.util.validation.OnlyNumbers;
import com.museum.projection.util.validation.OnlyText;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Object for Form validation
 */
public class FolderForm {

    @NotNull
    @NotEmpty
    @Size(max = 30)
    @OnlyText
    private String folder;

    @OnlyNumbers
    private String parentFolderId;

    public FolderForm(@NotNull @Size(max = 30) String folder, @OnlyNumbers String parentFolderId) {
        this.folder = folder;
        this.parentFolderId = parentFolderId;
    }

    public String getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(String parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
