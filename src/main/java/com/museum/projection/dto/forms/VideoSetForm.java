package com.museum.projection.dto.forms;

import com.museum.projection.util.validation.OnlyText;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Object for Form validation
 */
public class VideoSetForm {

    @NotNull
    @NotEmpty
    @Size(max = 30)
    @OnlyText
    private String name;


    @OnlyText
    private String description;

    public VideoSetForm(@NotNull @NotEmpty @Size(max = 30) String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
