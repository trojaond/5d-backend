package com.museum.projection.dto.forms;

import com.museum.projection.util.validation.OnlyFilename;
import com.museum.projection.util.validation.OnlyNumbers;
import com.museum.projection.util.validation.OnlyText;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Object for Form validation
 */
public class VideoFileForm {

    @NotNull
    @NotBlank
    @Size(max = 100)
    @OnlyFilename
    private String filename;

    @NotNull
    private MultipartFile file;

    @NotNull
    @NotEmpty
    @OnlyNumbers
    private String folder;

    @OnlyText
    @Size(max = 300)
    private String description;

    @NotNull
    private int trackType;

    private Integer duration;


    public VideoFileForm(@NotNull String filename, @NotNull MultipartFile file, @NotNull @NotEmpty String folder, String description, @NotNull int trackType, @OnlyNumbers Integer duration) {
        this.filename = filename;
        this.file = file;
        this.folder = folder;
        this.description = description;
        this.trackType = trackType;
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public int getTrackType() {
        return trackType;
    }

    public void setTrackType(int trackType) {
        this.trackType = trackType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
