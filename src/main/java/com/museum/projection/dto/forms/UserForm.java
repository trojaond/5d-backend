package com.museum.projection.dto.forms;

import com.museum.projection.util.validation.OnlyNumbers;
import com.museum.projection.util.validation.OnlyText;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Object for Form validation
 */
public class UserForm {

    @NotNull
    @Size(max = 30)
    @OnlyText
    private String username;

    @NotNull
    @Size(min = 6, max = 100)
    private String password;

    private String id;

    private boolean admin;

    private boolean presentator;

    private boolean active;


    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String toString() {
        return "User(Name: " + this.username + ", Password: " + this.password + ", IsActive: " + this.active + ", isAdmin, isPresentator: " + this.admin + ", " + this.presentator + ")";
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getPresentator() {
        return presentator;
    }

    public void setPresentator(Boolean presentator) {
        this.presentator = presentator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
