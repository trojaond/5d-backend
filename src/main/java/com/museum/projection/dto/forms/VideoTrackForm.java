package com.museum.projection.dto.forms;

import com.museum.projection.dto.other.VideoFileItem;
import com.museum.projection.util.validation.OnlyNumbers;
import com.museum.projection.util.validation.OnlyText;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;

/**
 * Object for Form validation
 */
public class VideoTrackForm {

    @NotNull
    @NotEmpty
    @Size(max = 30)
    @OnlyText
    private String name;

    @NotNull
    @NotEmpty
    @OnlyNumbers
    private String displaySetId;

    @Size(max = 300)
    @OnlyText
    private String description;


    @NotNull
    @NotEmpty(message = "List of videofiles must not be empty")
    private ArrayList<VideoFileItem> videoFilesIds;


    public VideoTrackForm(@NotNull @Size(max = 30) String name, @NotNull @NotEmpty @OnlyNumbers String displaySetId, @Size(max = 300) String description, @NotNull @NotEmpty(message = "List of videofiles must not be empty") ArrayList<VideoFileItem> videoFilesIds) {
        this.name = name;
        this.displaySetId = displaySetId;
        this.description = description;
        this.videoFilesIds = videoFilesIds;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<VideoFileItem> getVideoFilesIds() {
        return videoFilesIds;
    }

    public void setVideoFilesIds(ArrayList<VideoFileItem> videoFilesIds) {
        this.videoFilesIds = videoFilesIds;
    }

    public String getDisplaySetId() {
        return displaySetId;
    }

    public void setDisplaySetId(String displaySetId) {
        this.displaySetId = displaySetId;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}