
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class DisplaySetDto {
    private String id;
    private String name;
    private String createdByUsername;
    private String createdCet;
    private Boolean published;
    private int setAsAutonomous;
    private String status;
    private String duration;
    private boolean changed;
    private String description;

    public DisplaySetDto(String id, String name, String createdByUsername, String createdCet, boolean published, int setAsAutonomous, String duration, boolean changed, String description) {
        this.id = id;
        this.name = name;
        this.createdByUsername = createdByUsername;
        this.createdCet = createdCet;
        this.published = published;
        this.setAsAutonomous = setAsAutonomous;
        this.duration = duration;
        this.status = published
                ? changed
                    ? "Published but changed"
                    : "Published"
                : "Not published";
        this.changed = changed;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public String getCreatedCet() {
        return createdCet;
    }

    public Boolean isPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public int getSetAsAutonomous() {
        return setAsAutonomous;
    }

    public void setSetAsAutonomous(int setAsAutonomous) {
        this.setAsAutonomous = setAsAutonomous;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
