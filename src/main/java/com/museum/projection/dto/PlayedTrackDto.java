package com.museum.projection.dto;

import com.museum.projection.dto.other.ParsedPlaylistTrack;
import com.museum.projection.util.StringUtils;

import java.util.List;

/**
 * Data Transfer Object
 */
public class PlayedTrackDto {

    private String title;
    private String currentTimeFormated;
    private String lengthTimeFormated;
    private String displaySet;
    private String displayTrack;
    private boolean isPlaying;
    private int currentTime;
    private int length;
    private int percentage;
    private List<ParsedPlaylistTrack> playlist;

    public PlayedTrackDto(String title, String displaySet, String displayTrack, int currentTime, int length, boolean isPlaying, List<ParsedPlaylistTrack> playlist) {
        this.title = title;
        this.currentTimeFormated = StringUtils.getTimeFromSeconds(currentTime);
        this.lengthTimeFormated = StringUtils.getTimeFromSeconds(length);
        this.displaySet = displaySet;
        this.displayTrack = displayTrack;
        this.currentTime = currentTime;
        this.length = length;
        this.isPlaying = isPlaying;
        this.playlist = playlist;
        this.percentage = Math.round((currentTime * 100f) / length);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public String getCurrentTimeFormated() {
        return currentTimeFormated;
    }

    public void setCurrentTimeFormated(String currentTimeFormated) {
        this.currentTimeFormated = currentTimeFormated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPercentage() {
        return percentage;
    }

    public String getDisplaySet() {
        return displaySet;
    }

    public void setDisplaySet(String displaySet) {
        this.displaySet = displaySet;
    }

    public String getDisplayTrack() {
        return displayTrack;
    }

    public void setDisplayTrack(String displayTrack) {
        this.displayTrack = displayTrack;
    }

    public String getLengthTimeFormated() {
        return lengthTimeFormated;
    }

    public void setLengthTimeFormated(String lengthTimeFormated) {
        this.lengthTimeFormated = lengthTimeFormated;
    }

    public List<ParsedPlaylistTrack> getPlaylist() {
        return playlist;
    }

    public void setPlaylist(List<ParsedPlaylistTrack> playlist) {
        this.playlist = playlist;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }
}
