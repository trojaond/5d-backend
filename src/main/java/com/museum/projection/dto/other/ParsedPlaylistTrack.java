package com.museum.projection.dto.other;

public class ParsedPlaylistTrack {
    private boolean isRunning;
    private int index;
    private String name;
    private String displayTrackName;
    private String displayTrackId;

    public ParsedPlaylistTrack(boolean isRunning, int index, String name) {
        this.isRunning = isRunning;
        this.index = index;
        this.name = name;
    }


    public boolean isRunning() {
        return isRunning;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public String getDisplayTrackId() {
        return displayTrackId;
    }

    public void setDisplayTrackId(String displayTrackId) {
        this.displayTrackId = displayTrackId;
    }

    public String getDisplayTrackName() {
        return displayTrackName;
    }

    public void setDisplayTrackName(String displayTrackName) {
        this.displayTrackName = displayTrackName;
    }
}
