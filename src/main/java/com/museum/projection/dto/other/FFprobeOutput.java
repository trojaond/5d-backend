package com.museum.projection.dto.other;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.LinkedHashMap;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FFprobeOutput {
    public ArrayList<LinkedHashMap<String, Integer>> programs = new ArrayList<>();
    public ArrayList<LinkedHashMap<String, Integer>> streams = new ArrayList<>();
    Format FormatObject;


    // Getter Methods

    public Format getFormat() {
        return FormatObject;
    }

    // Setter Methods

    public void setFormat(Format formatObject) {
        this.FormatObject = formatObject;
    }

    public String getDuration(){
        return getFormat().getDuration();
    }
}
@JsonIgnoreProperties(ignoreUnknown = true)
class Format {
    private String filename;
    private float nb_streams;
    private float nb_programs;
    private String format_name;
    private String format_long_name;
    private String start_time;
    private String duration;
    private String size;
    private String bit_rate;
    private float probe_score;
    Tags TagsObject;


    // Getter Methods

    public String getFilename() {
        return filename;
    }

    public float getNb_streams() {
        return nb_streams;
    }

    public float getNb_programs() {
        return nb_programs;
    }

    public String getFormat_name() {
        return format_name;
    }

    public String getFormat_long_name() {
        return format_long_name;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getDuration() {
        return duration;
    }

    public String getSize() {
        return size;
    }

    public String getBit_rate() {
        return bit_rate;
    }

    public float getProbe_score() {
        return probe_score;
    }

    public Tags getTags() {
        return TagsObject;
    }

    // Setter Methods

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setNb_streams(float nb_streams) {
        this.nb_streams = nb_streams;
    }

    public void setNb_programs(float nb_programs) {
        this.nb_programs = nb_programs;
    }

    public void setFormat_name(String format_name) {
        this.format_name = format_name;
    }

    public void setFormat_long_name(String format_long_name) {
        this.format_long_name = format_long_name;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setBit_rate(String bit_rate) {
        this.bit_rate = bit_rate;
    }

    public void setProbe_score(float probe_score) {
        this.probe_score = probe_score;
    }

    public void setTags(Tags tagsObject) {
        this.TagsObject = tagsObject;
    }
}
@JsonIgnoreProperties(ignoreUnknown = true)
class Tags {
    private String major_brand;
    private String minor_version;
    private String compatible_brands;
    private String encoder;


    // Getter Methods

    public String getMajor_brand() {
        return major_brand;
    }

    public String getMinor_version() {
        return minor_version;
    }

    public String getCompatible_brands() {
        return compatible_brands;
    }

    public String getEncoder() {
        return encoder;
    }

    // Setter Methods

    public void setMajor_brand(String major_brand) {
        this.major_brand = major_brand;
    }

    public void setMinor_version(String minor_version) {
        this.minor_version = minor_version;
    }

    public void setCompatible_brands(String compatible_brands) {
        this.compatible_brands = compatible_brands;
    }

    public void setEncoder(String encoder) {
        this.encoder = encoder;
    }
}
