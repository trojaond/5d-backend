
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class DisplayTrackDto {
    private String id;
    private String displaySet;
    private String name;
    private String createdByUsername;
    private String createdCet;
    private String trackType;
    private String duration;
    private String description;


    public DisplayTrackDto(String id, String displaySet, String name, String createdByUsername, String createdCet, String trackType, String duration, String description) {
        this.id = id;
        this.displaySet = displaySet;
        this.name = name;
        this.createdByUsername = createdByUsername;
        this.createdCet = createdCet;
        this.trackType = trackType;
        this.duration = duration;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public String getDisplaySet() {
        return displaySet;
    }

    public String getCreatedCet() {
        return createdCet;
    }

    public String getTrackType() {
        return trackType;
    }

    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
