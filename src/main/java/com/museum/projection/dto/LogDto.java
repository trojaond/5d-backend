
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class LogDto {

    private String message;
    private String time;

    public LogDto(String message, String time) {
        this.message = message;
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}






