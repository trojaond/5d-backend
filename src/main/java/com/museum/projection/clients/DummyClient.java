package com.museum.projection.clients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *  Dummy client used, when schedulitn is set to false
 */
public class DummyClient implements RemoteClient {

    private static final Logger log = LoggerFactory.getLogger(DummyClient.class);
    private final int id;

    /**
     * Constructor
     * @param id
     */
    public DummyClient(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public int getId() {
        return id;
    }

    /**
     *
     * @param msg
     * @param expectedLines
     * @return
     */
    @Override
    public List<String> sendCmd(String msg, int expectedLines) {
        log.warn("Attempt to send command to " + id + " with VlcIntegration disabled");
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> sendForPlaylist() {
        return List.of("empty");
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isConnected() {
        return false;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isPlaying() {
        return false;
    }

    /**
     *
     * @throws InterruptedException
     */
    @Override
    public void maintainCheck() throws InterruptedException {

    }

    /**
     *
     * @throws InterruptedException
     */
    @Override
    public void initCheck() throws InterruptedException {

    }
}
