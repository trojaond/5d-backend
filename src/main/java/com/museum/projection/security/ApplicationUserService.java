package com.museum.projection.security;

import com.museum.projection.dao.IApplicationUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Application user service
 */
@Service
public class ApplicationUserService implements UserDetailsService {

    private final IApplicationUserDao applicationUserDao;

    @Autowired
    public ApplicationUserService(@Qualifier("postgres") IApplicationUserDao applicationUserDao) {
        this.applicationUserDao = applicationUserDao;
    }

    /**
     * Used by Spring security
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return applicationUserDao
                .getActiveApplicationUserByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("Username %s not found", username))
                );
    }
}
