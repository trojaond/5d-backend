package com.museum.projection.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.UserDto;
import com.museum.projection.dto.VideoFileDto;
import com.museum.projection.dto.forms.VideoFileForm;
import com.museum.projection.exception.StorageFileNotFoundException;
import com.museum.projection.model.TrackTypes;
import com.museum.projection.service.ModelViewService;
import com.museum.projection.service.StorageService;
import com.museum.projection.service.UserService;
import com.museum.projection.service.VideofileService;
import com.museum.projection.util.StringUtils;
import com.museum.projection.util.validation.OnlyNumbers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Controller for file upload and download
 */
@Controller
@Valid
@RequestMapping("/file")
public class TransferController {

    private final StorageService storageService;
    private final VideofileService videofileService;
    private final ModelViewService modelViewService;
    private final UserService userService;

    @Autowired
    public TransferController(StorageService storageService, VideofileService videofileService, ModelViewService modelViewService, UserService userService) {
        this.storageService = storageService;
        this.videofileService = videofileService;
        this.modelViewService = modelViewService;
        this.userService = userService;
    }

    @GetMapping(value = "/download/{videoId}")
    public ResponseEntity<StreamingResponseBody> download(@PathVariable("videoId") @OnlyNumbers String videoId, final HttpServletResponse response) {


        ResponseData<VideoFileDto> existingVideoFile = videofileService.getVideoFileById(videoId, true);
        if (existingVideoFile.getData() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        response.setContentType("application/zip");
        response.setHeader(
                "Content-Disposition",
                "attachment;filename=" + StringUtils.removeFileTypeExtension(existingVideoFile.getData().getFilename()) + ".zip");
        StreamingResponseBody stream = out -> {

            final File file = new File(existingVideoFile.getData().getFilepath());
            final ZipOutputStream zipOut = new ZipOutputStream(response.getOutputStream());
            if (file.exists()) {
                try {
                    final InputStream inputStream = new FileInputStream(file);
                    final ZipEntry zipEntry = new ZipEntry(file.getName());
                    zipOut.putNextEntry(zipEntry);
                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = inputStream.read(bytes)) >= 0) {
                        zipOut.write(bytes, 0, length);
                    }
                    inputStream.close();

                    zipOut.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        };
        return new ResponseEntity(stream, HttpStatus.OK);
    }


    @PostMapping("/upload")
    public String handleFileUpload(Principal principal, Model model, @Valid VideoFileForm
            videoFileForm, BindingResult bindingResult,
                                   RedirectAttributes redirectAttributes) {
        if (videoFileForm.getFile().isEmpty()) {
            bindingResult.addError(new FieldError("videoFileForm", "file", "Video must not be empty"));
        }
        if (bindingResult.hasErrors()) {
            modelViewService.populateVideoFiles(principal, model);
            return "videoedit/videofile";

        }
        ResponseData<UserDto> currentUser = userService.getUserByUsername(principal.getName());
        ResponseData<Boolean> response = null;
        //get type
        TrackTypes type = TrackTypes.of(videoFileForm.getTrackType());
        switch (type) {
            case picture: {
                response = storageService.storePicture(Integer.parseInt(currentUser.getData().getId()), videoFileForm);
                break;
            }
            case video: {
                response = storageService.storeAudioVideo(TrackTypes.video, Integer.parseInt(currentUser.getData().getId()), videoFileForm);
                break;
            }
            case audio: {
                response = storageService.storeAudioVideo(TrackTypes.audio, Integer.parseInt(currentUser.getData().getId()), videoFileForm);
                break;
            }
        }


        if (!response.getData()) {
            bindingResult.addError(new FieldError("videoFileForm", "file", response.getMessage()));
            modelViewService.populateVideoFiles(principal, model);
            return "videoedit/videofile";
        }
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + videoFileForm.getFile().getOriginalFilename() + "!");

        return "redirect:/control/videoedit/videofile";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}