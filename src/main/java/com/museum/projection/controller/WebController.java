package com.museum.projection.controller;

import com.museum.projection.config.AsyncConfiguration;
import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.*;
import com.museum.projection.dto.other.VideoFileItem;
import com.museum.projection.dto.forms.*;
import com.museum.projection.model.Log;
import com.museum.projection.model.TrackTypes;
import com.museum.projection.service.*;
import com.museum.projection.util.validation.OnlyNumbers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Web controller to which all front-end requests are directed
 */
@Controller
@Valid
public class WebController implements WebMvcConfigurer {

    @Resource()
    public VideofileService videofileService;
    @Resource()
    public ModelViewService modelViewService;
    @Resource()
    public DisplaySetService displaySetService;
    @Resource()
    public DisplayTrackService displayTrackService;
    @Resource
    public LogService logService;
    @Resource()
    public FolderService folderService;
    @Resource
    public StorageService storageService;
    @Resource
    public ClientService clientService;
    @Resource
    public SynchronizationService synchronizationService;
    @Resource()
    public UserService userService;
    @Resource
    public CustomConfig customConfig;


    private final Logger log = LoggerFactory.getLogger(WebController.class);

    /**
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    /**
     *
     * @param authentication
     * @param principal
     * @param model
     * @return
     */
    @GetMapping
    public String Index(Authentication authentication, Principal principal, Model model) {
        if (authentication != null) {
            modelViewService.populateMain(principal, model);
            return "main";
        }
        return "anonymous";
    }

    /**
     *
     * @return
     */
    @GetMapping("/login")
    public String Login() {
        return "login";
    }

    /**
     *
     * @param authentication
     * @return
     */
    @GetMapping("/logout")
    public String Logout(Authentication authentication) {
        authentication.setAuthenticated(false);
        return "anonymous";
    }

    /**
     *
     * @param model
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/logs", method = RequestMethod.GET)
    public String listLogs(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(40);

        Page<LogDto> logPage = logService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("logPage", logPage);
        model.addAttribute("activeTab", "logs");

        int totalPages = logPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "logs";
    }

//    Navigation

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    @GetMapping("/control")
    public String ControlMain(Principal principal, Model model) {
        modelViewService.populateHeader(principal, model);
        model.addAttribute("activeTab", "control");
        model.addAttribute("displaySets", displaySetService.getAllDisplaySets());
        model.addAttribute("clientCount", customConfig.getClientCount());
        return "control";
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    @GetMapping("/documentation")
    public String Documentation(Principal principal, Model model) {
        modelViewService.populateHeader(principal, model);
        model.addAttribute("activeTab", "documentation");
        return "documentation";
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    @GetMapping("/main")
    public String Main(Principal principal, Model model) {
        modelViewService.populateMain(principal, model);
        return "main";
    }

    /**
     *
     * @param principal
     * @param model
     * @param userForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/users")
    public String Users(Principal principal, Model model, UserForm userForm, BindingResult bindingResult) {
        modelViewService.populateHeader(principal, model);
        model.addAttribute("activeTab", "users");
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    @GetMapping("/track")
    public String PlayedTrack(Principal principal, Model model) {
        ResponseData<PlayedTrackDto> playedTrack = clientService.getPlayedTrack();
        model.addAttribute("track", playedTrack.getData());
        return "fragments/client :: playedTrack";
    }


    //    Users

    /**
     *
     * @param principal
     * @param model
     * @param userForm
     * @return
     */
    @GetMapping("/users/add")
    public String UserAdd(Principal principal, Model model, UserForm userForm) {
        return "fragments/userFragments :: create";
    }

    /**
     *
     * @param userForm
     * @param bindingResult
     * @param model
     * @param principal
     * @return
     */
    @PostMapping("/users/add")
    public String UserAddPost(@Valid UserForm userForm, BindingResult bindingResult, Model model, Principal principal) {
        if (bindingResult.hasErrors()) {
            modelViewService.populateUsersHeader(principal, model);
            model.addAttribute("addUser", "true");
            return "users";
        }
        ResponseData<Boolean> response = userService.createUser(userForm);
        if (!response.getData() && response.getField() != null) {
            bindingResult.addError(new FieldError("userForm", response.getField(), response.getMessage()));
        }
        if (bindingResult.hasErrors()) {
            modelViewService.populateUsersHeader(principal, model);
            model.addAttribute("addUser", "true");
            return "users";
        }
        return "redirect:/users";
    }

    /**
     *
     * @param userId
     * @param principal
     * @param model
     * @param userForm
     * @return
     */
    @GetMapping("/users/edit/roles/{userId}")
    public String UserEditRoles(@PathVariable("userId") @OnlyNumbers String userId, Principal principal, Model model, UserForm userForm) {
        ResponseData<UserDto> user = userService.getUserById(userId);
        return "fragments/userFragments :: edit_roles (username='" + user.getData().getName() + "',userId='" + user.getData().getId() + "')";
    }

    /**
     *
     * @param userId
     * @param principal
     * @param model
     * @param userForm
     * @return
     */
    @PostMapping("/users/edit/roles/{userId}")
    public String UserEditRolesP(@PathVariable("userId") @OnlyNumbers String userId, Principal principal, Model model, UserForm userForm) {
        ResponseData<Boolean> user = userService.updateRoles(userId, userForm);
        return user.getData() ? "redirect:/users" : "redirect:/users";  //todo ad errors
    }

    /**
     *
     * @param userId
     * @param principal
     * @param model
     * @param userForm
     * @return
     */
    @GetMapping("/users/edit/password/{userId}")
    public String UserEditPassword(@PathVariable("userId") @OnlyNumbers String userId, Principal principal, Model model, UserForm userForm) {
        ResponseData<UserDto> user = userService.getUserById(userId);
        return "fragments/userFragments :: edit_password (username='" + user.getData().getName() + "',userId='" + userId + "')";
    }

    /**
     *
     * @param userId
     * @param principal
     * @param model
     * @param userForm
     * @param bindingResult
     * @return
     */
    @PostMapping("/users/edit/password/{userId}")
    public String UserEditPasswordP(@PathVariable("userId") @OnlyNumbers String userId, Principal principal, Model model, @Valid UserForm userForm, BindingResult bindingResult) {
        if (bindingResult.getFieldError("password") != null) {
            ResponseData<UserDto> user = userService.getUserById(userId);
            if (user.getData() == null & user.getField() != null) {
                bindingResult.addError(new FieldError("userForm", user.getField(), user.getMessage()));
            }
            modelViewService.populateHeader(principal, model);
            model.addAttribute("activeTab", "users");
            model.addAttribute("users", userService.getAllUsers());
            model.addAttribute("editPassword", "true");
            model.addAttribute("editUsername", user.getData() == null ? "unknown" : user.getData().getName());
            model.addAttribute("editUserId", userId);
            return "users";
        }
        ResponseData<Boolean> updated = userService.updatePassword(userId, userForm);
        if (!updated.getData()) model.addAttribute("editPassword", "true");
        return "redirect:/users";
    }

    /**
     *
     * @param userId
     * @param principal
     * @param model
     * @return
     */
    @PostMapping("/users/edit/toggle/{userId}")
    public String UserEditActive(@PathVariable("userId") @OnlyNumbers String userId, Principal principal, Model model) {
        ResponseData<Boolean> user = userService.toggleActive(userId);
        if (user.getData() == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Foo Not Found");
        }
        modelViewService.populateHeader(principal, model);
        return "redirect:/users";
    }

    //    Control

    /**
     *
     * @param principal
     * @param model
     * @param autonomousForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/control/autonomous")
    public String ControlAutonomous(Principal principal, Model model, AutonomousForm autonomousForm, BindingResult bindingResult) {
        modelViewService.populateAutonomous(principal, model);
        return "autonomous";
    }

    /**
     *
     * @param principal
     * @param model
     * @param autonomousForm
     * @param bindingResult
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/control/autonomous/set")
    public String ControlAutonomousSet(Principal principal, Model model, AutonomousForm autonomousForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        //validate so there is no two display set with same order
        HashSet<Integer> order = new HashSet<>();
        for ( var displaySetItem: autonomousForm.getAutonomousSets()){
            if(displaySetItem.getOrder() == 0) continue;
            if(order.contains(displaySetItem.getOrder())) {
                bindingResult.addError(new FieldError("autonomousForm", "autonomousSets", "You cannot set same order of " + displaySetItem.getOrder() +" for multiple display sets "));
                modelViewService.populateAutonomous(principal, model);
                return "autonomous";
            }else {
                order.add(displaySetItem.getOrder());
            }

        }
        ResponseData<Boolean> set = displaySetService.setDefaultDisplaySet(autonomousForm.getAutonomousSets());
        modelViewService.populateAutonomous(principal, model);
        redirectAttributes.addFlashAttribute("message",
                "You successfully updated autonomous playlist");
        return "redirect:/control/autonomous";
    }

    /**
     *
     * @param principal
     * @param model
     * @param videoSetForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/control/videoedit")
    public String ControlVideoEdit(Principal principal, Model model, VideoSetForm videoSetForm, BindingResult bindingResult) {
        modelViewService.populateVideoSet(principal, model);
        return "videoedit/videoset";
    }

    /**
     *
     * @param principal
     * @param model
     * @param videoSetForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/control/videoedit/videoset")
    public String ControlVideoEditVideoSet(Principal principal, Model model, VideoSetForm videoSetForm, BindingResult bindingResult) {
        modelViewService.populateVideoSet(principal, model);
        return "videoedit/videoset";
    }

    /**
     *
     * @param displaySetId
     * @param principal
     * @param model
     * @return
     */
    @GetMapping("/control/videoedit/videoset/{displaySetId}")
    public String ControlVideoEditVideoSetDetail(@PathVariable("displaySetId") @OnlyNumbers String displaySetId, Principal principal, Model model) {
        model.addAttribute("displayTracksForDisplaySet", displayTrackService.getDisplayTracksByDisplaySetId(displaySetId));
        model.addAttribute("displaySet", displaySetService.getDisplaySetById(displaySetId).getData());
        return "fragments/videoSetFragments :: displaySetDetail";
    }

    /**
     *
     * @param videoSetForm
     * @param bindingResult
     * @param model
     * @param principal
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/control/videoedit/videoset/add")
    public String ControlVideoEditVideoSetAdd(@Valid VideoSetForm videoSetForm, BindingResult bindingResult, Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            modelViewService.populateVideoSet(principal, model);
            return "videoedit/videoset";
        }
        ResponseData<UserDto> currentUser = userService.getUserByUsername(principal.getName());

        //create videoTrack
        ResponseData<Boolean> responseData = displaySetService.createDisplaySet(currentUser.getData().getId(), videoSetForm.getName(),0.0f,videoSetForm.getDescription(), true );
        if (!responseData.getData() && responseData.getField() != null) {
            bindingResult.addError(new FieldError("videoSetForm", responseData.getField(), responseData.getMessage()));
        }
        if (bindingResult.hasErrors()) {
            modelViewService.populateVideoSet(principal, model);
            return "videoedit/videoset";
        }
        redirectAttributes.addFlashAttribute("message",
                "You successfully created video set " + videoSetForm.getName());
        return "redirect:/control/videoedit/videoset";
    }

    /**
     *
     * @param videoSetId
     * @param principal
     * @param model
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/control/videoedit/videoset/publish/{videoSetId}")
    public String ControlVideoEditVideoSetPublish(@PathVariable("videoSetId") @OnlyNumbers String videoSetId, Principal principal, Model model, RedirectAttributes redirectAttributes) {
        System.out.println("publish from " + videoSetId);
        ResponseData<Boolean> published = displaySetService.publishDisplaySet(videoSetId);
        if (!published.getData()) {
            redirectAttributes.addFlashAttribute("messageErr",
                    "Video set with id " + videoSetId + " cannot be published, " + published.getMessage());
        } else {
            redirectAttributes.addFlashAttribute("message",
                    "Video set with id " + videoSetId + " has been published");
        }

        return "redirect:/control/videoedit/videoset";
    }

    /**
     *
     * @param principal
     * @param model
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/control/videoedit/videoset/synchronize")
    public String ControlVideoEditVideoSetSynchronize(Principal principal, Model model, RedirectAttributes redirectAttributes) {
        log.info("synchronized");
        System.out.println("Thread: " + Thread.currentThread());
        ResponseData<Boolean> result = synchronizationService.synchronizeAll();
        if(!result.getData()){
            redirectAttributes.addFlashAttribute("messageErr", result.getMessage().isEmpty() ? "Unknown error" : result.getMessage());
        }else{
        redirectAttributes.addFlashAttribute("message", "Synchronization complete");

        }
        return "redirect:/control/videoedit/videoset";
    }

    /**
     *
     * @param principal
     * @param model
     * @param videoTrackForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/control/videoedit/videotrack")
    public String ControlVideoEditVideoTrack(Principal principal, Model model, VideoTrackForm videoTrackForm, BindingResult bindingResult) {
        modelViewService.populateVideoTrack(principal, model);
        return "videoedit/videotrack";
    }

    /**
     *
     * @param videoTrackForm
     * @param bindingResult
     * @param model
     * @param principal
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/control/videoedit/videotrack/add")
    public String ControlVideoEditVideoTrackAdd(@Valid VideoTrackForm videoTrackForm, BindingResult bindingResult, Model model, Principal principal, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            log.error("video track form has some errors");
            modelViewService.populateVideoTrack(principal, model);
            return "videoedit/videotrack";
        }
        ResponseData<UserDto> currentUser = userService.getUserByUsername(principal.getName());

        //create videoTrack
        ResponseData<Boolean> responseData = displayTrackService.createDisplayTrack(videoTrackForm.getDisplaySetId(), currentUser.getData().getId(), videoTrackForm.getName(), videoTrackForm.getDescription(), videoTrackForm.getVideoFilesIds().stream().map(VideoFileItem::getId).collect(Collectors.toList()));
        if (!responseData.getData() && responseData.getField() != null) {
            bindingResult.addError(new FieldError("videoTrackForm", responseData.getField(), responseData.getMessage()));
        }
        if (bindingResult.hasErrors()) {
            log.error("video track form has some errors");
            modelViewService.populateVideoTrack(principal, model);
            return "videoedit/videotrack";
        }
        redirectAttributes.addFlashAttribute("message",
                "You successfully created video track " + videoTrackForm.getName());
        return "redirect:/control/videoedit/videotrack";
    }

    /**
     *
     * @param displayTrackId
     * @param principal
     * @param model
     * @return
     */
    @GetMapping("/control/videoedit/videotrack/{displayTrackId}")
    public String ControlVideoEditVideoTrackDetail(@PathVariable("displayTrackId") @OnlyNumbers String displayTrackId, Principal principal, Model model) {

        model.addAttribute("videoFilesForDisplayTrack", videofileService.getAllVideoFilesByDisplayTrackId(displayTrackId, false));
        ResponseData<DisplayTrackDto> displayTrack = displayTrackService.getDisplayTrackById(displayTrackId);
        model.addAttribute("displayTrack", displayTrackService.getDisplayTrackById(displayTrackId).getData());
        return "fragments/videoTrackFragments :: displayTrackDetail";
    }

    /**
     *
     * @param principal
     * @param model
     * @param videoFileForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/control/videoedit/videofile")
    public String ControlVideoEditVideofile(Principal principal, Model model, VideoFileForm videoFileForm, BindingResult bindingResult) {
        modelViewService.populateVideoFiles(principal, model);
        return "videoedit/videofile";
    }

    //    VideoFolder

    /**
     *
     * @param principal
     * @param model
     * @param folderForm
     * @param bindingResult
     * @return
     */
    @GetMapping("/control/videoedit/videofolder")
    public String ControlVideoEditVideofolder(Principal principal, Model model, FolderForm folderForm, BindingResult bindingResult) {
        modelViewService.populateVideoFolder(principal, model);
        return "videoedit/videofolder";
    }

    /**
     *
     * @param folderForm
     * @param bindingResult
     * @param model
     * @param principal
     * @return
     */
    @PostMapping("/control/videoedit/videofolder/add")
    public String ControlVideoEditVideofolderAdd(@Valid FolderForm folderForm, BindingResult bindingResult, Model model, Principal principal) {
        if (bindingResult.hasErrors()) {
            modelViewService.populateVideoFolder(principal, model);
            return "videoedit/videofolder";
        }
        //validate
        ResponseData<Boolean> response = folderService.createFolder(Integer.parseInt(folderForm.getParentFolderId()), folderForm.getFolder());
        if (!response.getData() && response.getField() != null) {
            bindingResult.addError(new FieldError("folderForm", response.getField(), response.getMessage()));
        }
        if (bindingResult.hasErrors()) {
            modelViewService.populateVideoFolder(principal, model);
            return "videoedit/videofolder";
        }
        return "redirect:/control/videoedit/videofolder";
    }

    /**
     *
     * @param videoId
     * @param principal
     * @param model
     * @return
     */
    @PostMapping(value = "/control/videoedit/videofolder/delete/{videoId}")
    public String delete(@PathVariable("videoId") @OnlyNumbers String videoId, Principal principal, Model model) {
        ResponseData<VideoFileDto> existingVideoFile = videofileService.getVideoFileById(videoId, true);
        if (existingVideoFile.getData() == null) {
            modelViewService.populateVideoFiles(principal, model);
            model.addAttribute("messageErr", "Video with id " + videoId + " not found");
            return "videoedit/videofile";
        }
        ResponseData<Boolean> removed = storageService.removeVideoFile(videoId);
        if (!removed.getData()) {
            modelViewService.populateVideoFiles(principal, model);
            model.addAttribute("messageErr", removed.getMessage());
            return "videoedit/videofile";
        }
        return "redirect:/control/videoedit/videofile";
    }

}

