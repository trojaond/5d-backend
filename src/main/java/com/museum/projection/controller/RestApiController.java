package com.museum.projection.controller;

import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.DisplaySetDto;
import com.museum.projection.dto.DisplayTrackDto;
import com.museum.projection.dto.PlayedTrackDto;
import com.museum.projection.service.ClientService;
import com.museum.projection.service.DisplaySetService;
import com.museum.projection.service.DisplayTrackService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 *  REST API
 */
@RestController
@RequestMapping("/api")
public class RestApiController {

    @Resource
    private DisplaySetService displaySetService;
    @Resource
    private DisplayTrackService displayTrackService;
    @Resource
    private ClientService clientService;

    /**
     *
     * @return
     */
    @GetMapping("/test")
    public String get() {
        return "Test response from authorized endpoint";
    }

    /**
     *
     * @return
     */
    @GetMapping(value = "/displaysets", produces = "application/json")
    public List<DisplaySetDto> displaySets() {
        return displaySetService.getAllDisplaySets();
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/displaysets/{id}", produces = "application/json")
    public DisplaySetDto displaySetById(@PathVariable String id) {
        ResponseData<DisplaySetDto> found = displaySetService.getDisplaySetById(id);
        if (found.getData() == null)
            throw new ResponseStatusException(NOT_FOUND, "Unable to find display set with id " + id);
        return found.getData();
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/displaysets/{id}/displaytracks", produces = "application/json")
    public List<DisplayTrackDto> displayTracksOfdisplaySetById(@PathVariable String id) {
        ResponseData<DisplaySetDto> found = displaySetService.getDisplaySetById(id);
        if (found.getData() == null)
            throw new ResponseStatusException(NOT_FOUND, "Unable to find display set with id " + id);
        return displayTrackService.getDisplayTracksByDisplaySetId(found.getData().getId());
    }

    /**
     *
     * @param principal
     * @return
     */
    @GetMapping(value = "/user", produces = "application/json")
    public Map<String, String> currentUser(Principal principal){
        return  Map.of("username", principal.getName());
    }

    /**
     *
     * @return
     */
    @GetMapping(value = "/displaytracks", produces = "application/json")
    public List<DisplayTrackDto> displayTracks() {
        return displayTrackService.getAllDisplayTracks();
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/displaytracks/{id}", produces = "application/json")
    public DisplayTrackDto displayTrackById(@PathVariable String id) {
        ResponseData<DisplayTrackDto> found = displayTrackService.getDisplayTrackById(id);
        if (found.getData() == null)
            throw new ResponseStatusException(NOT_FOUND, "Unable to find display Track with id " + id);
        return found.getData();
    }

    /**
     *
     * @param principal
     * @return
     */
    @GetMapping(value = "/playedtrack", produces = "application/json")
    public PlayedTrackDto playedTrack(Principal principal){
        ResponseData<PlayedTrackDto> playedTrack = clientService.getPlayedTrack();
        if(playedTrack.getData() == null) throw new ResponseStatusException(SERVICE_UNAVAILABLE, "Unable to get current played track ");
        return  playedTrack.getData();
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/runautonomous", produces = "application/json")
    public ResponseEntity runAutonomous() {
        displaySetService.runAutonomous();
        return new ResponseEntity<>(
                "Autonomous playlist set to play", HttpStatus.OK);
    }

    /**
     *
     * @param ex
     * @return
     */
    @ExceptionHandler({ ResponseStatusException.class})
    public ResponseEntity<Map<String, Object>> handleException(ResponseStatusException ex) {
        Map<String,Object> ret = new HashMap<>();
        ret.put("error", ex.getReason());
        ret.put("timestamp", new Timestamp((new Date()).getTime()));
        ret.put("status",ex.getStatus());
        ResponseEntity< Map<String,Object>> entity = new ResponseEntity<>(ret, ex.getStatus());
        return  entity;
    }

}
