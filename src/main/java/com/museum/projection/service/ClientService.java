package com.museum.projection.service;

import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.*;
import com.museum.projection.dto.other.ParsedPlaylistTrack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Service managing client status
 */
@Service
public class ClientService {

    private final PlaylistService playlistService;
    private final ControlService controlService;
    private final DisplaySetService displaySetService;
    private final DisplayTrackService displayTrackService;
    private final VideofileService videofileService;
    private final CustomConfig config;

    private static final Logger log = LoggerFactory.getLogger(ClientService.class);

    /**
     *
     * @param playlistService
     * @param controlService
     * @param displaySetService
     * @param displayTrackService
     * @param videofileService
     * @param config
     */
    public ClientService(PlaylistService playlistService, ControlService controlService, DisplaySetService displaySetService, DisplayTrackService displayTrackService, VideofileService videofileService, CustomConfig config) {
        this.playlistService = playlistService;
        this.controlService = controlService;
        this.displaySetService = displaySetService;
        this.displayTrackService = displayTrackService;
        this.videofileService = videofileService;
        this.config = config;
    }

    /**
     * Return all clients
     * @return
     */
    public List<ClientDto> getAllClients() {
        List<ClientDto> clients = new ArrayList<>();
        for (int i = 0; i < config.getClientCount(); i++) {
            clients.add(new ClientDto(i + 1, config.getIp(i), config.getVlcPort(i), controlService.isConnected(i), controlService.isPlaying(i)));
        }
        return clients;
    }


    private boolean playing = false;
    private int prevTime;
    private int currentTime;
    private String currentTrack = "";
    private String currentDisplaySetName = "";
    private String currentDisplayTrackId = "";
    private String currentDisplayTrackName = "";
    private List<ParsedPlaylistTrack> currentPlaylistTracks = new ArrayList<>();
    private int currentTrackLength = 0;
    int connectedClient = Integer.MAX_VALUE;

    /**
     * Periodically queries playback status
     * @Scheduled
     * @throws InterruptedException
     */
    @Scheduled(fixedRate = 200)
    private void maintainCheck() throws InterruptedException {
        if (config.getQueryAll()) {
            for (var client : controlService.getConnectedClients()) {
                querySingleClient(client.getId());

            }
        } else {
            if (connectedClient == Integer.MAX_VALUE || !controlService.isConnected(connectedClient)) {
                //connect
                currentTrack = ""; //nobody connected
                connectedClient = Integer.MAX_VALUE;
                for (int i = 0; i < config.getClientCount(); i++) {
                    if (controlService.isConnected(i)) {
                        connectedClient = i;
                        break;
                    }
                }
                if (connectedClient == Integer.MAX_VALUE) {
                    log.info("Cannot find client to connect");
                    return;
                }
            }
            querySingleClient(connectedClient);
        }


    }

    /**
     * Retreives playback status from single node
     * @param clientId
     */
    private void querySingleClient(int clientId) {
        currentTime = controlService.getTime(clientId);
        long end = System.currentTimeMillis();
        currentTrack = controlService.getTitle(clientId);
        if (currentTrack == null || currentTrack.equals("")) {
            log.info("Client " + clientId + ": No title to parse, title equals empty or null string");
            return;
        }
        String[] split = currentTrack.split("-");
        if (split.length != 3) {
            log.error("Client " + clientId + ": Title " + currentTrack + " cannot be splitted to 3 items");
            return;
        }
//        DisplaySet
        ResponseData<DisplaySetDto> displaySet = displaySetService.getDisplaySetById(split[0]);
        if (displaySet.getData() == null) {
            log.error("Client " + clientId + ": Cannot find displaySet with id " + split[0]);
            return;
        }
        currentDisplaySetName = displaySet.getData().getName();

//        DisplayTrack
        ResponseData<DisplayTrackDto> displayTrack = displayTrackService.getDisplayTrackById(split[1]);
        if (displayTrack.getData() == null) {
            log.error("Client " + clientId + ": Cannot find displayTrack with id " + split[1]);
            return;
        }
        if (!currentDisplayTrackId.equals(displayTrack.getData().getId()) || currentPlaylistTracks.isEmpty()) {
            //check playlist
            List<ParsedPlaylistTrack> displayTracks = new ArrayList<>();
            List<ParsedPlaylistTrack> tracks = playlistService.parseTitlesPlaylist(controlService.getPlaylist(clientId));
            for (ParsedPlaylistTrack parsedTrack : tracks) {
                ResponseData<DisplayTrackDto> displayTrackFromParsed = displayTrackService.getDisplayTrackById(parsedTrack.getName().split("-")[1]);
                if (displayTrackFromParsed.getData() == null) {
                    displayTracks.add(new ParsedPlaylistTrack(parsedTrack.isRunning(), parsedTrack.getIndex(), "unknown displayTrack"));
                    continue;
                }
                parsedTrack.setDisplayTrackId(displayTrackFromParsed.getData().getId());
                parsedTrack.setDisplayTrackName(displayTrackFromParsed.getData().getName());
                displayTracks.add(parsedTrack);
            }
            currentPlaylistTracks = displayTracks;
            currentDisplayTrackId = displayTrack.getData().getId();
            currentDisplayTrackName = displayTrack.getData().getName();
        }
        currentTrackLength = controlService.getLength(clientId);
        log.debug(LocalDateTime.now().toString() + " " + currentDisplaySetName + ", " + currentDisplayTrackName + ", " + split[2]);
    }

    /**
     * Returns playback status in a form of PlayedTrackDTO object
     * @return
     */
    public ResponseData<PlayedTrackDto> getPlayedTrack() {
        //get currentTrack
        boolean connected = false;
        for (int i = 0; i < config.getClientCount(); i++) {
            if (controlService.isConnected(i)) {
                connected = true;
                break;
            }
        }
        if (!connected) return new ResponseData<>(null);

        if (currentTrack == null || currentTrack.equals("")) return new ResponseData<>(new PlayedTrackDto("unknown",
                "unknown", "unknown", 0,
                0, playing, List.of()));
//        int currentTime = controlService.getTime(connectedClient);
        playing = currentTime != prevTime;
        prevTime = currentTime;
        return new ResponseData<>(new PlayedTrackDto(currentTrack,
                currentDisplaySetName, currentDisplayTrackName, currentTime,
                currentTrackLength, playing, currentPlaylistTracks));

    }


}
