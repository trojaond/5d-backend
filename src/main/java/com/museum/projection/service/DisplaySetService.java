package com.museum.projection.service;


import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.DisplaySetDaoService;
import com.museum.projection.dao.DisplayTrackToVideoFileDaoService;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.DisplaySetDto;
import com.museum.projection.dto.DisplayTrackDto;
import com.museum.projection.dto.other.DisplaySetItem;
import com.museum.projection.dto.other.PlaylistTrack;
import com.museum.projection.model.DisplaySet;
import com.museum.projection.model.Videofile;
import com.museum.projection.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * DisplaySet Business Service
 */
@Service
public class DisplaySetService {

    private final DisplaySetDaoService displaySetDaoService;
    private final DisplayTrackToVideoFileDaoService displayTrackToVideoFileDaoService;

    private final DisplayTrackService displayTrackService;
    private final PlaylistService playlistService;
    private final UserService userService;
    private final ControlService controlService;

    private final CustomConfig config;
    private boolean startedDefautlPlaylist;

    private static final Logger log = LoggerFactory.getLogger(DisplaySetService.class);

    @Autowired
    public DisplaySetService(DisplaySetDaoService displaySetDaoService, DisplayTrackToVideoFileDaoService displayTrackToVideoFileDaoService, DisplayTrackService displayTrackService, PlaylistService playlistService, UserService userService, ControlService controlService, CustomConfig config) {
        this.displaySetDaoService = displaySetDaoService;
        this.displayTrackToVideoFileDaoService = displayTrackToVideoFileDaoService;
        this.displayTrackService = displayTrackService;
        this.playlistService = playlistService;
        this.userService = userService;
        this.controlService = controlService;
        this.config = config;
    }

    /**
     * On a bootup starts autonomous if all clients are connected
     * @Scheduled
     */
    @Scheduled(fixedRate = 1000)
    private void setDefaultDisplaySet() {
        if (controlService.areAllConnected() && !startedDefautlPlaylist) {
            runAutonomous();
            startedDefautlPlaylist = true;
        }
    }

    /**
     * Send autonomous playlist sequence to play
     */
    public void runAutonomous() {
        log.info("Set autonomous playlist to start");
        List<DisplaySetDto> orderedAutonomousPlaylist = getOrderedAutonomousDisplaySets();
        if (orderedAutonomousPlaylist.isEmpty()) {
            log.error("No autonomous playlist set");
            return;
        }
        controlService.addPlaylist(orderedAutonomousPlaylist.get(0).getName());
        for (int i = 1; i < orderedAutonomousPlaylist.size(); i++) {
            controlService.enqueuePlaylist(orderedAutonomousPlaylist.get(i).getName());
        }
    }

    /**
     * Returns display sets
     * @return
     */
    public List<DisplaySetDto> getAllDisplaySets() {
        List<DisplaySetDto> displaySets = displaySetDaoService.getDisplaySets().stream().map(displaySet -> new DisplaySetDto(
                displaySet.getId().toString(),
                displaySet.getName(),
                userService.getUserById(String.valueOf(displaySet.getCreatedBy())).getData().getName(),
                StringUtils.toDateTime(displaySet.getCreatedCet()),
                displaySet.getPublished(),
                displaySet.getAutonomous(),
                displaySet.getDuration().toString(),
                displaySet.getChanged(),
                displaySet.getDescription())).collect(Collectors.toList());
        return displaySets;
    }

    /**
     *
     * @return
     */
    public List<DisplaySetDto> getOrderedAutonomousDisplaySets() {
        return displaySetDaoService.getDisplaySets().stream().filter(displaySet -> displaySet.getAutonomous() != 0).sorted((d1, d2) -> d1.getAutonomous().compareTo(d2.getAutonomous())).map(displaySet -> new DisplaySetDto(
                displaySet.getId().toString(),
                displaySet.getName(),
                userService.getUserById(String.valueOf(displaySet.getCreatedBy())).getData().getName(),
                StringUtils.toDateTime(displaySet.getCreatedCet()),
                displaySet.getPublished(),
                displaySet.getAutonomous(),
                displaySet.getDuration().toString(),
                displaySet.getChanged(),
                displaySet.getDescription())).collect(Collectors.toList());
    }

    /**
     *
     * @param currentUserId
     * @param name
     * @param duration
     * @param description
     * @param changed
     * @return
     */
    public ResponseData<Boolean> createDisplaySet(String currentUserId, String name, Float duration, String description, boolean changed) {
        //create videoTrack
        Optional<Long> createdDisplaySetId = displaySetDaoService.insertDisplaySet(name, Integer.parseInt(currentUserId), false, 0, duration, description, changed);
        if (createdDisplaySetId.isEmpty()) {
            return new ResponseData<>(false, "display set could not be created");
        }
        return new ResponseData<>(true);
    }

    /**
     *
     * @param displaySetId
     * @return
     */
    public ResponseData<DisplaySetDto> getDisplaySetById(String displaySetId) {
        ResponseData<DisplaySetDto> ret;
        try {
            ret = displaySetDaoService.getDisplaySetById(Integer.parseInt(displaySetId)).map(displaySet ->
                    new ResponseData<>(new DisplaySetDto(
                            displaySet.getId().toString(),
                            displaySet.getName(),
                            userService.getUserById(displaySet.getCreatedBy().toString()).getData().getName(),
                            StringUtils.toDateTime(displaySet.getCreatedCet()),
                            displaySet.getPublished(),
                            displaySet.getAutonomous(),
                            displaySet.getDuration().toString(),
                            displaySet.getChanged(),
                            displaySet.getDescription()))).orElseThrow();

        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(null, String.format("Display set with id %s not found", displaySetId), "displaySet");
        }
        return ret == null ? new ResponseData<>(null, "unkown error") : ret;
    }

    /**
     *
     * @param listOfDisplaySets
     * @return
     */
    public ResponseData<Boolean> setDefaultDisplaySet(ArrayList<DisplaySetItem> listOfDisplaySets) {
        HashMap<Integer, Integer> displaySetIdToOrder = new HashMap<>();
        for (int i = 0; i < listOfDisplaySets.size(); i++) {
            if (listOfDisplaySets.get(i).getOrder() == 0) continue;
            Optional<DisplaySet> displaySet = displaySetDaoService.getDisplaySetById(i);
            if (displaySet.isEmpty()) return new ResponseData<>(true, "No display set for id found");
            displaySetIdToOrder.put(i, listOfDisplaySets.get(i).getOrder());
        }

        for (DisplaySet displaySet : displaySetDaoService.getDisplaySets()) {
            if (displaySetIdToOrder.containsKey(displaySet.getId())) {
                displaySetDaoService.updateDisplaySet(displaySet.getId().toString(), displaySet.getName(), displaySet.getPublished(), displaySetIdToOrder.get(displaySet.getId()), displaySet.getChanged(), displaySet.getDuration());
            } else if (displaySet.getAutonomous() != 0) {
                displaySetDaoService.updateDisplaySet(displaySet.getId().toString(), displaySet.getName(), displaySet.getPublished(), 0, displaySet.getChanged(), displaySet.getDuration());
            }
        }
        //generate playlist
        return new ResponseData<>(true);
    }

    /**
     * Publish, validates the display set, generated playlist and if successful marks the display set as published
     * @param displaySetId
     * @return
     */
    public ResponseData<Boolean> publishDisplaySet(String displaySetId) {
        Optional<DisplaySet> displaySet = displaySetDaoService.getDisplaySetById(Integer.parseInt(displaySetId));
        if (displaySet.isEmpty()) return new ResponseData<>(false, "No display set for id " + displaySetId + " found");
        //get display tracks
        List<DisplayTrackDto> displayTracks = displayTrackService.getDisplayTracksByDisplaySetId(displaySetId);
        if (displayTracks.isEmpty())
            return new ResponseData<>(false, "No display track attached to display set, cannot publish");

        int clientCount = config.getClientCount();
        ArrayList<PlaylistTrack>[] playlistsData = new ArrayList[clientCount];

        //verify if synchronized
        boolean allSynchronized;
        for (DisplayTrackDto displayTrack : displayTracks) {
            List<Videofile> videoFilesForDisplayTrack = displayTrackToVideoFileDaoService.getVideoFilesByDisplayTrackId(Integer.parseInt(displayTrack.getId()));
            if (videoFilesForDisplayTrack.size() != clientCount)
                return new ResponseData<>(false, "Display track " + displayTrack.getName() + " has incorect amount of videofiles, expected:" + clientCount + " found:" + videoFilesForDisplayTrack.size());
            boolean displayTrackSynchronized = videoFilesForDisplayTrack.stream().map(Videofile::getClientSynchronized).reduce(true, (a, b) -> a && b);
            if (!displayTrackSynchronized) {
                return new ResponseData<>(false, "Display track " + displayTrack.getName() + " has not synchronized videofile, run synchronization");
            }
            for (int i = 0; i < clientCount; i++) {
                Videofile video = videoFilesForDisplayTrack.get(i);
                if (playlistsData[i] == null) playlistsData[i] = new ArrayList<PlaylistTrack>();
                playlistsData[i].add(new PlaylistTrack(video.getFilepath(), video.getFilename(), video.getDuration(), displayTrack.getName(), displayTrack.getId()));
            }

        }

        //generate playlist for each client
        ResponseData<String[]> playlistsCreated = playlistService.generatePlaylists(displaySet.get().getName(), displaySet.get().getId().toString(), playlistsData);
        if (playlistsCreated.getData().length == 0) {
            return new ResponseData<>(false, "Playlists could not have been created");
        }
        playlistService.savePlaylist(displaySet.get().getName(), playlistsCreated.getData());
        //location, duration


        //set as published
        displaySetDaoService.updateDisplaySet(displaySetId, displaySet.get().getName(), true, displaySet.get().getAutonomous(), false, displaySet.get().getDuration());

        return new ResponseData<>(true);
    }

}
