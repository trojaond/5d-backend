package com.museum.projection.service;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.FolderDto;
import com.museum.projection.dto.VideoFileDto;
import com.museum.projection.dto.forms.VideoFileForm;
import com.museum.projection.dto.other.FFprobeOutput;
import com.museum.projection.model.TrackType;
import com.museum.projection.model.TrackTypes;
import com.museum.projection.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * Storage service
 */
@Service
public class StorageService {

    private static final Logger log = LoggerFactory.getLogger(StorageService.class);
    private final CustomConfig config;
    private final String videoFolderPath;
    private final VideofileService videofileService;
    private final FolderService folderService;

    @Autowired
    public StorageService(CustomConfig config, VideofileService videofileService, FolderService folderService) {
        this.config = config;
        this.videoFolderPath = config.getVideoFolderPath();
        this.videofileService = videofileService;
        this.folderService = folderService;
    }

    /**
     * Delete video file
     * @param videoFileId
     * @return
     */
    public ResponseData<Boolean> removeVideoFile(String videoFileId) {
        ResponseData<VideoFileDto> existing = videofileService.getVideoFileById(videoFileId, true);
        String filePath = existing.getData().getFilepath();
        File file = new File(filePath);
        boolean flag = false;
        if (!file.exists()) return new ResponseData<>(false, "Physical file does  not exist");
        flag = file.delete();
        if (!flag) return new ResponseData<>(false, "Physical file cannot be deleted");
        return new ResponseData<Boolean>(videofileService.removeVideoFileDbItem(videoFileId).getData());
    }

    /**
     * Validates, and stores audio-video files
     * @param type
     * @param currentUserId
     * @param form
     * @return
     */
    public ResponseData<Boolean> storeAudioVideo(TrackTypes type, int currentUserId, VideoFileForm form) {
        MultipartFile file = form.getFile();
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (file.isEmpty()) {
            log.error("Uploaded file is empty");
        }
        String filePath = "";
        try {
            ResponseData<FolderDto> folder = folderService.getFolderById(form.getFolder());
            if (folder.getData() == null) {
                return new ResponseData<>(false, "Folder error");
            }
            filePath = addEndSlashIfMissing(StringUtils.cutLastSlash(videoFolderPath) + folder.getData().getFolder()) + form.getFilename();
            File dest = new File(filePath);
            if (dest.exists()) {
                return new ResponseData<>(false, "File with same name already exist");
            }
            log.info("saving uploaded file to = {}", filePath);
            File incoming = new File("/tmp/incoming");
            file.transferTo(incoming); //transfer file to folder


            String[] command = {"ffmpeg", "-v", "quiet", "-i", incoming.getAbsolutePath(), "-map_metadata", "-1", "-c:v", "copy", "-c:a", "copy", filePath};
            ProcessBuilder ffmpeg = processBuilder.command(command);
            int exitCode = 0;
            try {
                var process = ffmpeg.start();
                exitCode = process.waitFor();
            } catch (IOException | InterruptedException e) {
                log.error("Error processing incoming file - removing metadata");
            }
            incoming.delete();
            if (exitCode != 0) {
                //cleanup
                return new ResponseData<>(false, "Error processing incoming file");
            }

        } catch (IOException e) {
            log.error("Error with file transfer", e);
            return new ResponseData<>(false, "File error");
        }
        boolean errorRemoveFile = false;
        //transfered check videofile
        String[] command = {"ffprobe", "-v", "quiet", "-print_format", "json", "-show_format", "-show_entries", "stream=width,height", filePath};
        ProcessBuilder ffprobe = processBuilder.command(command);
        File outFile = new File("/tmp/out");
        ffprobe.redirectOutput(outFile);
        int exitCode = 0;
        try {
            var process = ffprobe.start();
            exitCode = process.waitFor();
        } catch (IOException | InterruptedException e) {
            errorRemoveFile = true;
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        FFprobeOutput output = null;
        try {
            output = objectMapper.readValue(outFile, FFprobeOutput.class);
        } catch (IOException e) {
            errorRemoveFile = true;
            log.error("Error parsing video file output", e);
        }
        if (output == null) {
            log.error("Error parsing video file output");
            errorRemoveFile = true;
        }
        outFile.delete();
        if (errorRemoveFile || output.getDuration() == null) {
            File uploaded = new File(filePath);
            uploaded.delete();
            return new ResponseData<>(false, "Error parsing video file output");
        }
        if (type == TrackTypes.video) {
            int width = output.streams.get(0).get("width");
            int height = output.streams.get(0).get("height");
        }
        float duration = Float.parseFloat(output.getDuration());

        videofileService.createVideoFile(currentUserId, filePath, form.getFilename(), form.getDescription(), true, duration, file.getSize(), Integer.parseInt(form.getFolder()), type);
        return new ResponseData<>(true);
    }

    /**
     * Validates and stores picture files
     * @param currentUserId
     * @param form
     * @return
     */
    public ResponseData<Boolean> storePicture(int currentUserId, VideoFileForm form) {
        MultipartFile file = form.getFile();
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (file.isEmpty()) {
            log.error("Uploaded file is empty");
        }
        String filePath = "";
        try {
            ResponseData<FolderDto> folder = folderService.getFolderById(form.getFolder());
            if (folder.getData() == null) {
                return new ResponseData<>(false, "Folder error");
            }
            filePath = addEndSlashIfMissing(StringUtils.cutLastSlash(videoFolderPath) + folder.getData().getFolder()) + form.getFilename();
            File dest = new File(filePath);
            if (dest.exists()) {
                return new ResponseData<>(false, "File with same name already exist");
            }
            log.info("saving uploaded file to = {}", filePath);
            file.transferTo(dest); //transfer file to folder

        } catch (IOException e) {
            log.error("Error with file transfer", e);
            return new ResponseData<>(false, "File error");
        }

        //transfered
        float duration = form.getDuration() == null ? 10f: form.getDuration();

        videofileService.createVideoFile(currentUserId, filePath, form.getFilename(), form.getDescription(), true, duration, file.getSize(), Integer.parseInt(form.getFolder()), TrackTypes.picture);
        return new ResponseData<>(true);
    }


    private String addEndSlashIfMissing(String path) {
        if (!path.endsWith("/")) {
            path = path + '/';
        }
        return path;
    }


}