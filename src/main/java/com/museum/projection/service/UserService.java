package com.museum.projection.service;


import com.museum.projection.dao.IApplicationUserDao;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.UserDto;
import com.museum.projection.dto.forms.UserForm;
import com.museum.projection.model.Account;
import com.museum.projection.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * User service
 */
@Service
public class UserService {

    private final IApplicationUserDao applicationUserDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(@Qualifier("postgres") IApplicationUserDao applicationUserDao, PasswordEncoder passwordEncoder) {
        this.applicationUserDao = applicationUserDao;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     *
     * @return
     */
    public List<UserDto> getAllUsers() {
        var test = applicationUserDao.getAllAccount();
        List<UserDto> applicationUsers = applicationUserDao.getAllAccount().stream().map(account -> new UserDto(
                String.valueOf(account.getId()),
                account.getUsername(),
                String.valueOf(account.getIsEnabled()),
                account.getRoles(),
                StringUtils.toDateTime(account.getCreatedCet()))).collect(Collectors.toList());
        return applicationUsers;
    }

    /**
     *
     * @param id
     * @return
     */
    public ResponseData<UserDto> getUserById(String id) {
        ResponseData<UserDto> ret;

        try {
            ret = applicationUserDao.getAccountByUserId(Integer.parseInt(id)).map(account ->
                    new ResponseData<>(new UserDto(account.getId().toString(),
                            account.getUsername(),
                            String.valueOf(account.getIsEnabled()),
                            account.getRoles(), StringUtils.toDateTime(account.getCreatedCet())))
            ).orElseThrow();
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(null, String.format("User with id %s not found", id), "userId");
        }
        return ret == null ? new ResponseData<>(null, "unkown error") : ret;
    }

    /**
     *
     * @param username
     * @return
     */
    public ResponseData<UserDto> getUserByUsername(String username) {
        ResponseData<UserDto> ret;

        try {
            ret = applicationUserDao.getAccountByUsername(username).map(account ->
                    new ResponseData<>(new UserDto(account.getId().toString(),
                            account.getUsername(),
                            String.valueOf(account.getIsEnabled()),
                            account.getRoles(), StringUtils.toDateTime(account.getCreatedCet())))
            ).orElseThrow();
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(null, String.format("User with username %s not found", username), "username");
        }
        return ret == null ? new ResponseData<>(null, "unkown error") : ret;
    }

    /**
     *
     * @param userForm
     * @return
     */
    public ResponseData<Boolean> createUser(UserForm userForm) {
        ResponseData<Boolean> ret;
        try {
            ret = new ResponseData<>(applicationUserDao.insertAccount(userForm.getUsername(),
                    passwordEncoder.encode(userForm.getPassword()),
                    userForm.getPresentator() && userForm.getAdmin() ? String.join(",", "ADMIN", "PRESENTATOR") :
                            userForm.getAdmin() ? "ADMIN" : userForm.getPresentator() ? "PRESENTATOR" : "",
                    userForm.getActive()).get());
        } catch (DuplicateKeyException e) {
            return new ResponseData(false, "username already exists", "username");
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;

    }

    /**
     *
     * @param userForm
     * @return
     */
    public ResponseData<Boolean> updateUser(UserForm userForm) {
        ResponseData<Boolean> ret;
        try {
            ret = new ResponseData(applicationUserDao.updateAccount(Integer.valueOf(userForm.getId()),
                    userForm.getUsername(),
                    passwordEncoder.encode(userForm.getPassword()),
                    mergeRoles(userForm.getAdmin(), userForm.getPresentator()),
                    userForm.getActive()));
        } catch (DuplicateKeyException e) {
            return new ResponseData(false, "username already exists", "username");
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(false, String.format("User with id %s not found", userForm.getId()), "userId");
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;
    }

    /**
     *
     * @param userId
     * @return
     */
    public ResponseData<Boolean> toggleActive(String userId) {
        ResponseData<Boolean> ret;
        Account existing = null;
        try {
            existing = applicationUserDao.getAccountByUserId(Integer.valueOf(userId)).orElseThrow();
        } catch (EmptyResultDataAccessException | NoSuchElementException e) {
            return new ResponseData<>(false, String.format("User with id %s not found", userId, "userId"));
        }
        try {
            ret = new ResponseData(applicationUserDao.updateAccount(existing.getId(), existing.getUsername(), existing.getPassword(), existing.getRoles(), !existing.getIsEnabled()).orElseThrow());
        } catch (DuplicateKeyException e) {
            return new ResponseData(false, "username already exists", "username");
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(false, String.format("User with id %s not found", userId, "userId"));
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;
    }

    /**
     *
     * @param userId
     * @param userForm
     * @return
     */
    public ResponseData<Boolean> updateRoles(String userId, UserForm userForm) {
        ResponseData<Boolean> ret;
        Account existing;
        try {
            existing = applicationUserDao.getAccountByUserId(Integer.valueOf(userId)).orElseThrow();
        } catch (EmptyResultDataAccessException | NoSuchElementException e) {
            return new ResponseData<>(false, String.format("user with id %s not found", userId, "userId"));
        }
        try {
            ret = new ResponseData(applicationUserDao.updateAccount(existing.getId(), existing.getUsername(), existing.getPassword(), mergeRoles(userForm.getAdmin(), userForm.getPresentator()), existing.getIsEnabled()).orElseThrow());
        } catch (DuplicateKeyException e) {
            return new ResponseData(false, "username already exists", "username");
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(false, String.format("user with id %s not found", userId, "userId"));
        } catch (IllegalArgumentException e) {
            return new ResponseData(false, String.format("id %s is invalid", userId, "userId"));
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;
    }

    /**
     *
     * @param userId
     * @param userForm
     * @return
     */
    public ResponseData<Boolean> updatePassword(String userId, UserForm userForm) {
        ResponseData<Boolean> ret;
        Account existing;
        try {
            existing = applicationUserDao.getAccountByUserId(Integer.valueOf(userId)).orElseThrow();
        } catch (EmptyResultDataAccessException | NoSuchElementException e) {
            return new ResponseData<>(false, String.format("User with id %s not found", userId), "userId");
        }
        try {
            ret = new ResponseData(applicationUserDao.updateAccount(existing.getId(), existing.getUsername(), passwordEncoder.encode(userForm.getPassword()), existing.getRoles(), existing.getIsEnabled()).orElseThrow());
        } catch (DuplicateKeyException e) {
            return new ResponseData(false, "username already exists", "username");
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(false, String.format("user with id %s not found", userId, "userId"));
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;
    }


    private String mergeRoles(boolean isAdmin, boolean isPresentator) {
        return isPresentator && isAdmin ? String.join(",", "ADMIN", "PRESENTATOR") :
                isAdmin ? "ADMIN" : isPresentator ? "PRESENTATOR" : "";
    }

}
