package com.museum.projection.service;


import com.museum.projection.config.CustomConfig;
import com.museum.projection.clients.RemoteClient;
import com.museum.projection.dto.VideoFileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.ConnectionLostException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Control Service manages remote clients. Intermediary for commands sending
 */
@Service
public class ControlService {

    private static final int INTERVAL_MAINTAIN = 4000;
    private static final int INTERVAL_STARTUP = 1000;

    private static volatile boolean outOfSync = false;
    private static final Logger log = LoggerFactory.getLogger(ControlService.class);

    private final CustomConfig config;
    private final LogService logService;
    private final int clientCount;

    @Resource(name = "clients")
    private List<RemoteClient> clients;

    @Autowired
    public ControlService(CustomConfig config, LogService logService) {
        this.config = config;
        this.clientCount = config.getClientCount();
        this.logService = logService;
    }

    private RemoteClient getRemoteClient(int id) {
        try {
            return clients.get(id);
        } catch (IndexOutOfBoundsException ex) {
            log.error("client with id " + id + " is not present in the client list", ex);
            throw ex;
        }
    }

    /**
     * @Scheduled
     * @throws InterruptedException
     */
    @Scheduled(fixedRate = INTERVAL_STARTUP)
    public void initCheck() throws InterruptedException {
        clients.forEach(remoteClient -> {
            try {
                remoteClient.initCheck();
            } catch (InterruptedException e) {
                log.error("Error during connection init", e);
            }
        });

    }

    /**
      @Scheduled
     * @throws InterruptedException
     */
    @Scheduled(fixedRate = INTERVAL_MAINTAIN)
    public void maintainCheck() throws InterruptedException {
        clients.forEach(remoteClient -> {
            try {
                remoteClient.maintainCheck();
            } catch (InterruptedException e) {
                log.error("Error during connection maintain", e);
            }
        });
    }


    private List<String> sendCommand(RemoteClient client, String cmd) {
        var ret = sendCommand(client, cmd, "");
        if (ret == null || ret.size() > getLinesFor(cmd)) {
            log.error("Invalid response for command " + cmd);
            return List.of();
        }
        return ret;
    }

    private List<String> sendCommand(RemoteClient client, String cmd, String parameter) {
        return client.sendCmd(cmd + " " + parameter, getLinesFor(cmd));
    }

    public boolean areAllConnected() {
        for (RemoteClient client : clients) {
            if (!client.isConnected()) return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public List<RemoteClient> getConnectedClients(){
        return clients.stream().filter(RemoteClient::isConnected).collect(Collectors.toList());
    }

    /**
     *
     * @param clientId
     * @return
     */
    public boolean isConnected(int clientId) {
        return getRemoteClient(clientId).isConnected();
    }

    /**
     *
     * @param clientId
     * @return
     */
    public boolean isPlaying(int clientId) {
        return getRemoteClient(clientId).isPlaying();
    }

    /**
     *
     * @param clientId
     * @return
     */
    public List<String> getPlaylist(int clientId) {
        return getRemoteClient(clientId).sendForPlaylist();
    }

    /**
     *
     * @param clientId
     * @return
     */
    public String getTitle(int clientId) {
        String command = "get_title";
        List<String> ret = sendCommand(getRemoteClient(clientId), command);
        if (ret == null || ret.isEmpty()) return "";
        return ret.get(0);
    }

    /**
     *
     * @param clientId
     * @return
     */
    public int getTime(int clientId) {
        String command = "get_time";
        List<String> ret = sendCommand(getRemoteClient(clientId), command);
        if (ret == null || ret.isEmpty() || ret.get(0).equals("")) return 0;
        return Integer.parseInt(ret.get(0).replaceAll("\\s+", ""));
    }

    /**
     *
     * @param clientId
     * @return
     */
    public int getLength(int clientId) {
        String command = "get_length";
        List<String> ret = sendCommand(getRemoteClient(clientId), command);
        if (ret == null || ret.isEmpty() || ret.get(0).equals("")) return 0;
        return Integer.parseInt(ret.get(0));
    }


    public void play() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "play");
        }
        logService.createLog("Clients set to play");
    }

    public void next() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "next");
        }
        logService.createLog("Clients set to next");
    }

    public void prev() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "prev");
        }
        logService.createLog("Clients set to prev");
    }

    public void repeat() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "repeat");
        }
        logService.createLog("Clients set to repeat");
    }

    public void shuffle() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "random");
        }
        logService.createLog("Clients set to shuffle");
    }

    /**
     *
     * @param itemId
     */
    public void gotoitem(int itemId) {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "goto", Integer.toString(itemId));
        }
        logService.createLog("Clients set to go to item " + itemId);
    }

    /**
     *
     * @param playlistIndexItemToMove
     * @param playlistIndexMoveAfter
     */
    public void move(int playlistIndexItemToMove, int playlistIndexMoveAfter) {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "move", Integer.toString(playlistIndexItemToMove) + " " + playlistIndexMoveAfter);
        }
        logService.createLog("Clients set to move item " + playlistIndexItemToMove + " behind " + playlistIndexMoveAfter);
    }

    /**
     *
     * @param second
     */
    public void seek(int second) {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "seek", Integer.toString(second));
        }
        logService.createLog("Clients set to seek " + second);
    }

    /**
     *
     * @param displaySetId
     * @param displayTrackId
     * @param fileNames
     */
    public void addVideo(String displaySetId, String displayTrackId, List<VideoFileDto> fileNames) {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "add", PlaylistService.toServerFileName(config.getVideoFolderPath(), displaySetId, displayTrackId, fileNames.get(i).getFilename()));
        }
        logService.createLog("DisplayTrack with id" + displayTrackId + "  added");
    }

    /**
     *
     * @param displaySetId
     * @param displayTrackId
     * @param fileNames
     */
    public void enqueueVideo(String displaySetId, String displayTrackId, List<VideoFileDto> fileNames) {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "enqueue", PlaylistService.toServerFileName(config.getVideoFolderPath(), displaySetId, displayTrackId, fileNames.get(i).getFilename()));
        }
        logService.createLog("DisplayTrack with id" + displayTrackId + "  enqueued");
    }

    /**
     *
     * @param playlistName
     */
    public void addPlaylist(String playlistName) {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "clear");
            //TODO remove this from dev
            sendCommand(getRemoteClient(i), "add", config.getPlaylistFolderPath().replace("otrojan", "user") + PlaylistService.getPlaylistName(playlistName));
        }
        logService.createLog("Playlist " + playlistName + " has been set to play");
    }

    /**
     *
     * @param playlistName
     */
    public void enqueuePlaylist(String playlistName) {
        for (int i = 0; i < clientCount; i++) {
            //TODO remove this from dev
            sendCommand(getRemoteClient(i), "enqueue", config.getPlaylistFolderPath().replace("otrojan", "user") + PlaylistService.getPlaylistName(playlistName));
        }
        logService.createLog("Playlist " + playlistName + " has been enqueued");
    }

    /**
     *
     * @param clientId
     */
    public void readCurrentPlaylist(int clientId) {
        sendCommand(getRemoteClient(clientId), "playlist");
    }

    public void pause() {
        boolean res = true;
        for (int i = 0; i < clientCount; i++) {
            List<String> pause = sendCommand(getRemoteClient(i), "pause");
        }
        logService.createLog("Clients set to pause");
    }

    public void stop() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "stop");
        }
        logService.createLog("Clients set to stop");
    }

    public void clear() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "clear");
        }
    }

    public void logoutAll() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "logout");
        }
    }

    /**
     *
     * @param id
     */
    public void shutdownById(String id) {
        int localId = Integer.parseInt(id)-1;
        sendCommand(getRemoteClient(localId), "shutdown");
    }

    public void shutdownAll() {
        for (int i = 0; i < clientCount; i++) {
            sendCommand(getRemoteClient(i), "shutdown");
        }
    }

    /**
     * Returns number of expected lines for a command
     * @param command
     * @return
     */
    private int getLinesFor(String command) {
        switch (command) {
            case "add":
            case "play":
            case "clear":
            case "pause":
            case "next":
            case "prev":
            case "repeat":
            case "goto":
            case "random":
            case "seek":
            case "move":
            case "enqueue":
                return 0;

            case "is_playing":
            case "logout":
            case "shutdown":
            case "get_title":
            case "get_time":
            case "get_length":
                return 1;
            default:
                return Integer.MAX_VALUE;
        }
    }

}
