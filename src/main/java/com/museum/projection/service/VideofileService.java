package com.museum.projection.service;


import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.DisplayTrackToVideoFileDaoService;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dao.VideofileDaoService;
import com.museum.projection.dto.VideoFileDto;
import com.museum.projection.model.TrackTypes;
import com.museum.projection.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Video file service
 */
@Service
public class VideofileService {

    private final VideofileDaoService filenameDao;
    private final DisplayTrackToVideoFileDaoService displayTrackToVideoFileDaoService;

    private final UserService userService;

    private final CustomConfig config;
    private final String prefixPath;


    @Autowired
    public VideofileService(VideofileDaoService applicationUserDao, DisplayTrackToVideoFileDaoService displayTrackToVideoFileDaoService, UserService userService, CustomConfig config) {
        this.filenameDao = applicationUserDao;
        this.displayTrackToVideoFileDaoService = displayTrackToVideoFileDaoService;
        this.userService = userService;
        this.config = config;
        this.prefixPath = config.getVideoFolderPath();
    }

    /**
     *
     * @param id
     * @param fullPath
     * @return
     */
    public ResponseData<VideoFileDto> getVideoFileById(String id, boolean fullPath) {
        ResponseData<VideoFileDto> ret;

        try {
            ret = filenameDao.getVideoFileById(Integer.parseInt(id)).map(file ->
                    new ResponseData<>(new VideoFileDto(
                            file.getId().toString(),
                            file.getFilename(),
                            fullPath ? file.getFilepath() : '/' + org.apache.commons.lang3.StringUtils.removeStart(file.getFilepath(), prefixPath),
                            StringUtils.humanReadableByteCountSI(file.getFileSize()),
                            file.getDuration().toString(),
                            file.getDescription(),
                            StringUtils.toDateTime(file.getCreatedCet()),
                            userService.getUserById(file.getCreatedBy().toString()).getData().getName(),
                            file.getClientSynchronized().toString(),
                            TrackTypes.of(file.getVideoFileTypeId()).toString()))
            ).orElseThrow();
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(null, String.format("Videofile with id %s not found", id), "videofile");
        }
        return ret == null ? new ResponseData<>(null, "unkown error") : ret;
    }

    /**
     *
     * @return
     */
    public List<VideoFileDto> getAllVideoFiles() {
        List<VideoFileDto> videoFileDtoList = filenameDao.getVideoFiles().stream().map(file -> new VideoFileDto(
                file.getId().toString(),
                file.getFilename(),
                '/' + org.apache.commons.lang3.StringUtils.removeStart(file.getFilepath(), prefixPath),
                StringUtils.humanReadableByteCountSI(file.getFileSize()),
                file.getDuration().toString(),
                file.getDescription(),
                StringUtils.toDateTime(file.getCreatedCet()),
                userService.getUserById(file.getCreatedBy().toString()).getData().getName(),
                file.getClientSynchronized().toString(),
                TrackTypes.of(file.getVideoFileTypeId()).toString())).collect(Collectors.toList());
        return videoFileDtoList;
    }

    /**
     *
     * @param currentUserId
     * @param filePath
     * @param filename
     * @param description
     * @param clientSynchronized
     * @param duration
     * @param size
     * @param folderId
     * @param trackType
     * @return
     */
    public ResponseData<Boolean> createVideoFile(int currentUserId, String filePath, String filename, String description, boolean clientSynchronized, float duration, long size, int folderId, TrackTypes trackType) {
        ResponseData<Boolean> ret;
        try {
            ret = new ResponseData<>(filenameDao.insertVideoFile(filename, filePath, description, currentUserId, clientSynchronized, duration, size, folderId,trackType.getId()).orElse(false));
        } catch (DuplicateKeyException e) {
            return new ResponseData(false, "videofile already exists", "username");
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;
    }

    /**
     *
     * @param videoFileId
     * @return
     */
    public ResponseData<Boolean> removeVideoFileDbItem(String videoFileId) {
        return new ResponseData<>(filenameDao.removeVideoFile(Integer.parseInt(videoFileId)).orElse(false));
    }

    /**
     *
     * @param displayTrackId
     * @param fullPath
     * @return
     */
    public List<VideoFileDto> getAllVideoFilesByDisplayTrackId(String displayTrackId, boolean fullPath) {
        List<VideoFileDto> videoFileDtoList = displayTrackToVideoFileDaoService.getVideoFilesByDisplayTrackId(Integer.parseInt(displayTrackId)).stream().map(file -> new VideoFileDto(
                file.getId().toString(),
                file.getFilename(),
                fullPath ? file.getFilepath() : '/' + org.apache.commons.lang3.StringUtils.removeStart(file.getFilepath(), prefixPath),
                StringUtils.humanReadableByteCountSI(file.getFileSize()),
                file.getDuration().toString(),
                file.getDescription(),
                StringUtils.toDateTime(file.getCreatedCet()),
                userService.getUserById(file.getCreatedBy().toString()).getData().getName(),
                file.getClientSynchronized().toString(),
                TrackTypes.of(file.getVideoFileTypeId()).toString())).collect(Collectors.toList());
        return videoFileDtoList;
    }

}
