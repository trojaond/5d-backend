import {serverUrl, getCookie, sendHttpRequest} from './common.js';

let addUserContainer = document.querySelector("#addUserContainer")

let addUserBtn = document.querySelector("#addUserBtn");
if (addUserBtn) {
    addUserBtn.addEventListener('click', () => {
        sendHttpRequest('GET', serverUrl + '/users/add')
            .then(responseData => {
                document.querySelector("#addUserContainer").insertAdjacentHTML('beforeend', responseData);
                hideAddButton()
            }).catch(err => {
            console.error("error" + err);
        })
    })
}

let editUserBtns = document.querySelectorAll(".user-edit-roles")
editUserBtns.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('GET', serverUrl + '/users/edit/roles/' + this.value)
        .then(responseData => {
            addUserContainer = document.querySelector("#addUserContainer")
            addUserContainer.innerHTML = "";
            clearEditContainer()
            hideAddButton()
            addUserContainer.insertAdjacentHTML('beforeend', responseData);
        }).catch(err => {
        console.error("error" + err);
    })
}));
let editUserPasswordBtns = document.querySelectorAll(".user-edit-password")
editUserPasswordBtns.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('GET', serverUrl + '/users/edit/password/' + this.value)
        .then(responseData => {
            addUserContainer = document.querySelector("#addUserContainer")
            addUserContainer.innerHTML = "";
            clearEditContainer()
            hideAddButton()
            addUserContainer.insertAdjacentHTML('beforeend', responseData);
        }).catch(err => {
        console.error("error" + err);
    })
}));
let editUserToggleBtns = document.querySelectorAll(".user-edit-toggle")
editUserToggleBtns.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('POST', serverUrl + '/users/edit/toggle/' + this.value)
        .then(responseData => {
            location.reload();
        }).catch(err => {
        console.error("error" + err);
    })
}));


function hideAddButton() {
    const addUserBtn = document.querySelector("#addUserBtn");
    if (addUserBtn) addUserBtn.hidden = true;
}

function clearEditContainer() {
    const editPasswordContainer = document.querySelector("#editUserContainer");
    if (editPasswordContainer) editPasswordContainer.innerHTML = ""
}
