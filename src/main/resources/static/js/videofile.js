import {serverUrl, getCookie, sendHttpRequest} from './common.js';

let filenameInput = document.querySelector("input#inputFilename");
let fileInput = document.querySelector("input.custom-file-input");
let uploadVideoContent = document.querySelector("div#uploadVideoContent");
let videoSizeText = document.querySelector("p#videoSize");
let uploadVideoFormElement = document.querySelector("form#uploadVideoForm");
let uploadVideoSubmitButton = document.querySelector("button#uploadFileSubmitButton")
if (fileInput) {
    fileInput.addEventListener('change', function () {
        filenameInput.classList.remove('is-invalid');
        fileInput.labels[0].classList.remove('is-invalid');
        let file = this.files[0];
        filenameInput.value = file.name;
        fileInput.labels[0].innerHTML = file.name;
        let fileSize = file.size;
        let maxFileSize = Number(uploadVideoFormElement.getAttribute("maxFileSize"));
        if (fileSize >= maxFileSize) {
            videoSizeText.innerText = "File is too large " + fileSize.fileSize(1) + " / " + maxFileSize.fileSize(1);
            videoSizeText.classList.remove("text-muted");
            videoSizeText.classList.add("text-danger");
            uploadVideoSubmitButton.disabled = true;
        } else {
            videoSizeText.innerText = fileSize.fileSize(1) + " / " + maxFileSize.fileSize(1);
            videoSizeText.classList.remove("text-danger");
            videoSizeText.classList.add("text-muted");
            uploadVideoSubmitButton.disabled = false;
        }
    })
}
let uploadVideoForm = document.querySelector("form#uploadVideoForm");
if (uploadVideoForm) {
    uploadVideoForm.addEventListener('submit', function () {
        uploadVideoContent.hidden = true;
        uploadVideoContent.insertAdjacentHTML('afterend', "<div class=\"spinner-border m-5\" role=\"status\">\n" +
            "  <span class=\"sr-only\">Loading...</span>\n" +
            "</div>")
    })
}
let videosDeleteButton = document.querySelectorAll(".videofile-delete")
videosDeleteButton.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('POST', serverUrl + '/control/videoedit/videofolder/delete/' + this.value.toString())
        .then(responseData => {
            location.reload();
        }).catch(err => {
        console.error("error" + err);
    })
}));

let selectedTypeButton = document.querySelector("#fileTypeSelect");
selectedTypeButton.addEventListener('change', function (event) {
    document.querySelector("#durationFormField").hidden = event.target.value !== "1";
});



Object.defineProperty(Number.prototype, 'fileSize', {
    value: function (a, b, c, d) {
        return (a = a ? [1e3, 'k', 'B'] : [1024, 'K', 'iB'], b = Math, c = b.log,
                d = c(this) / c(a[0]) | 0, this / b.pow(a[0], d)).toFixed(2)
            + ' ' + (d ? (a[1] + 'MGTPEZY')[--d] + a[2] : 'Bytes');
    }, writable: false, enumerable: false
});