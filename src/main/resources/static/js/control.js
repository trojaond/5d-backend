import {serverUrl, getCookie, sendHttpRequest} from './common.js';

var playBtn = document.getElementById('play');
var pauseBtn = document.getElementById('pause');
var nextBtn = document.getElementById('next');
var prevBtn = document.getElementById('prev');
var stopBtn = document.getElementById('stop');
var shutdown1Btn = document.getElementById('shutdown1');
var shutdown2Btn = document.getElementById('shutdown2');
var autonomous  = document.getElementById('runAutonomous');

var apiVlcPrefix = '/api/vlc';


function updatePlayedTrack() {
    sendHttpRequest('GET', serverUrl + '/track')
        .then(responseData => {
            let container = document.querySelector("div#playedContent");
            container.innerHTML = "";
            container.insertAdjacentHTML('beforeend', responseData);
        }).catch(err => {

    })
}

window.setInterval(updatePlayedTrack, 500);

let addPlaylist = document.querySelectorAll(".change-playlist")
addPlaylist.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('POST', serverUrl + apiVlcPrefix + '/addplaylist/' + this.value)
        .then(responseData => {
        }).catch(err => {
    })
}));

function controlEvent(event) {
    sendHttpRequest('POST', serverUrl + apiVlcPrefix + '/' + event).then(responseData => {
        handeResponseAlert(responseData);
    }).catch(err => {
    })
}



function controlShutdown(btn, client) {
    sendHttpRequest('POST', serverUrl + apiVlcPrefix + '/shutdown/' + client)
        .then(responseData => {
            handeResponseAlert(responseData);
        }).catch(err => {
    })
}

let shutdownButtons = document.querySelectorAll(".shutdown")
shutdownButtons.forEach(button => button.addEventListener('click', function () {
    controlShutdown(shutdown1Btn, this.value);
}));

function handeResponseAlert(text) {
    console.log(text);
    // alertMessageBox.insertAdjacentHTML('beforeend', '<div class="alert alert-success" role="alert">'+text+'</div>');
}

stopBtn.addEventListener('click', () => controlEvent("stop"));
playBtn.addEventListener('click', () => controlEvent("play"));
nextBtn.addEventListener('click', () => controlEvent("next"));
prevBtn.addEventListener('click', () => controlEvent("prev"));
pauseBtn.addEventListener('click', () => controlEvent("pause"));
autonomous.addEventListener('click', function () {
    console.log('autonomous');
    sendHttpRequest('POST', serverUrl + '/api/runautonomous').then(responseData => {
        handeResponseAlert(responseData);
    }).catch(err => {
    })
});
