import {serverUrl, getCookie, sendHttpRequest} from './common.js';


function updatePlayedTrack() {
    sendHttpRequest('GET', serverUrl + '/track')
        .then(responseData => {
            let container = document.querySelector("div#playedContent");
            container.innerHTML = "";
            container.insertAdjacentHTML('beforeend', responseData);
        }).catch(err => {
        console.error("error" + err);
    })
}

window.setInterval(updatePlayedTrack, 800);