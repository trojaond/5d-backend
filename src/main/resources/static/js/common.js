document.querySelector('#logout').addEventListener('click', () =>{
    document.cookie = 'Authorization=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.location.href="/";
})

export var serverUrl = 'http://' + window.location.host;

export function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

export const sendHttpRequest = (method, url, data) => {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url)
        xhr.setRequestHeader('Authorization', getCookie("Authorization"));
        if (data) {
            xhr.setRequestHeader('Content-Type', 'application/json');
        }
        xhr.onload = () => {
            if (xhr.status > 400) {
                reject(xhr.response)
            } else {
                resolve(xhr.response)
            }
        };
        xhr.onerror = () => {
            reject('Something went wrong check the backend log')
        };

        xhr.send(JSON.stringify(data));

    });
    return promise;
}