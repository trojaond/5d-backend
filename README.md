# 5D Projection backend
This repository uses 

Spring Boot
(postgres)




## Getting started

### Build
Then build source using located maven runing
```bash
./mvnw clean package -Dmaven.test.skip=true
```

### Using docker

Spring boot requires running postgres db. For initial start run as stated below
```docker run --name 5d-postgres-manual -e POSTGRES_PASSWORD=password -d -p 5432:5432  postgres:alpine```

Then just start with

```docker start 5d-postgres-manual```

For the first time, you need to create a database. Connect to running container with
```docker exec -it 5d-postgres-manual bash```
and in container connecto to postgres
```psql -U postgres```
and run SQL command 
```CREATE DATABASE projection;```



Then generate .jar file as is shown in Dockerfile. Then run as follows:
```
docker build -t spring .
&& docker run
-p 8080:8080 -p 5041:5041 -p 5031:5031
--name spring
--network host
spring 
```



### Running VLC Instances
The server application requires running instances of VLC with RC interface. Such application is available at

https://gitlab.fel.cvut.cz/trojaond/5d.git
